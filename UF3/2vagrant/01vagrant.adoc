:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

= Vagrant

Vagrant és una eina de codi obert per a la creació i configuració d'entorns
virtualitzats. Es va desenvolupar originalment per VirtualBox i sistemes de
"provisioning" com Chef, Salt o Puppet.

Actualment és capaç de treballar amb d'altres proveïdors com ara VMWare, Amazon
AWS, LXC i DigitalOcean.

Vagrant comença com un projecte personal de Mitchell Hashimoto al *2010*, la
primera versió apareix aquest mateix any i la primera versió estable al *2012*.

Mitchell Hashimoto funda HashiCorp al 2012 dedicada a la creació d'eines comercials
i suport al voltant de Vagrant.

== Usos

Crear escenaris virtuals de forma senzilla i reproduïble.

* Entorns de proves
* Entorns de producció simples, desenvolupament i sistemes

Existeix una versió de Vagrant desenvolupada per la creació d'escenaris grans de
Vagrant anomenada *Terraform*.

== Característiques

Bona integració amb eines de gestió de configuració (_Configuration Management
Systems_) Puppet, Chef, Ansible, Salt.

S'emmarca dins del corrent de _Gestió de la infraestructura com a codi_.

Problemes:

* Configurar escenaris a mà és pesat.
* Els escenaris poden canviar.
* Els desenvolupadors han de centrar-se en el desenvolupament.

Limitacions:

* Configuracions que depenen de l'hypervisor.
* Escenaris complexos.

== Instal·lació

https://www.vagrantup.com/

Necessita un sistema de virtualització. Es va desenvolupar en VirtualBox i es
recomana aquest sistema amb el qual té una perfecta integració.

[source]
----
vagrant -v
Vagrant 2.3.4
----

== Ajuda

El paràmetre *_-h_*
[source]
----
vagrant -h
----

[source]
----
vagrant ssh -h
----

=== "Vagrant Box"

Una "box" és un format específic de fitxer amb una màquina virtual i metadades;
la màquina virtual hauria d'estar configurada segons les recomanacions establertes per Vagrant:

* Integració amb SSH.
* Usuari amb permisos i amb permisos SSH anomenat *_vagrant_* amb password *_vagrant_*.
* Sudo sense password:
+
[source, bash]
----
vagrant ALL=(ALL) NOPASSWD: ALL
----
* Gestor de paquets configurat
* ...

Inicialment la pròpia empresa Hashicorp distribuïa els boxes, actualment les
pròpies distribucions tenen els seus boxes penjats.

Trobem boxes de dos tipus:

base boxes:: Conté la mínima funcionalitat perquè Vagrant pugui funcionar.
boxes preconfigurades:: Poden contenir software preinstal·lat com ara servidors Web o gestors de base de dades.

El *nom de les boxes* segueix el següent format:

[source]
----
nom_usuari/nom_box
----

== Vagrant Cloud

La manera més senzilla d'obtenir una "Vagrant box" és a través del web *Vagrant cloud*.

https://app.vagrantup.com/boxes/search

Podem baixar directament un box des de Vagrant Cloud fent

[source]
----
vagrant box add centos/7
----

Si el host és Windows, els boxes baixats s'emmagatzemen a `c:\users\usuari\.vagrant.d`,
es pot canviar aquesta ubicació movent la carpeta i afegint variable d'entorn a
*_VAGRANT_HOME = d:\dev\.vagrant.d_*

En Linux, els boxes es guarden a `~/.vagrant.d`.

Per veure les box que tenim baixades:

[source]
----
vagrant box list
----

== Escenaris de Vagrant

Un escenari de Vagrant consisteix en descriure les imatges requerides per un
projecte junt amb la seva configuració i aprovisionament.

Els escenaris estan dirigits per un fitxer *_Vagrantfile_* amb sintaxi _Ruby_.

== Creació d'escenaris de Vagrant

Cada escenari a la seva pròpia carpeta i el seu fitxer *_Vagrantfile_*

Entrem a la carpeta (A partir d'ara n'hi direm *_escenari_*).

I creem un nou *_Vagrantfile_*:

* Creem un fitxer _Vagrantfile_ i una imatge a partir d'un box:
+
[source]
----
vagrant init nom_box
----
* El paràmetre *_-m_* generarà un *_Vagrantfile_* mínim.
+
[source]
----
vagrant init -m nom_box
----

== Gestionar escenaris

Un cop definit un escenari es pot iniciar amb:

[source]
----
vagrant up
----

Per defecte es genera un escenari amb un Vagrantfile minimal:

.Exemple de fitxer Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/trusty64"
end
----

Tindrà una màquina Ubuntu + Xarxa interna NAT

Podem accedir a la màquina virtual amb:

[source]
----
vagrant ssh
----

I veure'n l'estat amb:

[source]
----
vagrant status
----

Parar la màquina:

[source]
----
vagrant halt
----

Eliminar la màquina definitivament:

[source]
----
vagrant destroy
----

=== Imatges preconfigurades

Existeixen imatges preconfigurades però tenen alguns inconvenients:

* Són molt pesades.
* Algunes són oficials, i d'altres no. No tenim la garantia de què hi ha instal·lat.
* Tenim solucions més òptimes, però més complexes, si el que volem és una imatge amb un servei
preconfigurat, per exemple docker.

=== Característiques de les imatges base

Les imatges base en VirtualBox solen tenir, *per defecte*, un conjunt de
característiques comunes:

* Connexió *_ssh_* configurada amb parell de claus al port 2222.
+
La clau privada resideix dins del directori *_.\.vagrant\machines\debianc\virtualbox_*
dins de l'escenari. Ho podem saber amb `vagrant ssh-config`.
* Connexió de xarxa de tipus NAT amb IP dinàmica.
* Usuari *_vagrant_* amb contrasenya *_vagrant_* i permissos de *_sudo_*.
* *_sudo_* sense password.
* És desitjable que tinguin les *_Guest additions_* instal·lades, en cas contrari
algunes no disposarem d'algunes funcionalitats.
* El punt de muntatge *_/vagrant_* apunta a la pròpia carpeta de l'escenari al host.

== Vagrant Boxes

Vagrant denomina *box* a un paquet que conté la imatge d'una màquina virtual amb
el format adequat per a un determinat proveïdor i alguns fitxers amb metadades.

Podem trobar-ne de preconfigurades a: https://app.vagrantup.com/boxes/search

[WARNING]
====
Qualsevol usuari pot pujar "boxes" al cloud de Vagrant.
====

=== Reempaquetar un Box

Les imatges baixades es troben dins del directori `.vagrant.d\boxes\nom_box\num_versio\hypervisor`

[source]
----
joan@debian:~/.vagrant.d/boxes$ ls -l
total 8
drwxr-xr-x 3 joan joan 4096 18 oct.  2022 debian-VAGRANTSLASH-bullseye64
drwxr-xr-x 3 joan joan 4096  2 maig 11:46 ubuntu-VAGRANTSLASH-xenial64
joan@debian:~/.vagrant.d/boxes$ cd debian-VAGRANTSLASH-bullseye64/11.20220912.1/virtualbox/
joan@debian:~/.vagrant.d/boxes/debian-VAGRANTSLASH-bullseye64/11.20220912.1/virtualbox$ ls
box.ovf  box_update_check  box.vmdk  metadata.json  Vagrantfile
----

Quan afegim un box, aquest es descomprimeix i el fitxer original s'esborra. Podem reempaquetar
un box amb la següent comanda:

[source]
----
joan@debian:~$ vagrant box list
debian/bullseye64 (virtualbox, 11.20230501.1)
joan@debian:~$ vagrant box repackage debian/bullseye64 virtualbox 11.20230501.1
joan@debian:~$ ls *.box
package.box
----

[IMPORTANT]
====
Falten les capacitats de versionat que ofereix la pàgina de Vagrant Cloud.
====

=== Eliminar i instal·lar box

[source]
----
D:\vagrantProjects>vagrant box list
debian/stretch64 (virtualbox, 9.8.0)
ubuntu/trusty64  (virtualbox, 20190401.0.0)

D:\vagrantProjects>vagrant box remove debian/stretch64
Removing box 'debian/stretch64' (v9.8.0) with provider 'virtualbox'...

D:\vagrantProjects>vagrant box list
ubuntu/trusty64 (virtualbox, 20190401.0.0)
----

Perdem el versionat

[source]
----
D:\vagrantProjects>vagrant box add package.box --name debian/stretch64
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'debian/stretch64' (v0) for provider:
    box: Unpacking necessary files from: file://D:/vagrantProjects/package.box
    box: Progress: 100% (Rate: 1252M/s, Estimated time remaining: --:--:--)
==> box: Successfully added box 'debian/stretch64' (v0) for 'virtualbox'!

D:\vagrantProjects>vagrant box list
debian/stretch64 (virtualbox, 0)
ubuntu/trusty64  (virtualbox, 20190401.0.0)
----

Pot ser útil per passar-lo a un màquina sense connexió, per exemple.

=== Actualització d'imatges

Podem instal·lar una versió desactualitzada:

----
D:\vagrantProjects>vagrant box add centos/7 --box-version 1811.02
----

[source, cmd]
----
D:\vagrantProjects>vagrant box outdated  -h
Usage: vagrant box outdated [options]

Checks if there is a new version available for the box
that are you are using. If you pass in the --global flag,
all boxes will be checked for updates.

Options:

        --global                     Check all boxes installed
        --insecure                   Do not validate SSL certificates
        --cacert FILE                CA certificate for SSL download
        --capath DIR                 CA certificate directory for SSL download
        --cert FILE                  A client SSL cert, if needed
    -h, --help                       Print this help
----

[source]
----
D:\vagrantProjects>vagrant box outdated  --global
* 'ubuntu/trusty64' for 'virtualbox' is outdated! Current: 20190401.0.0. Latest:
 20190402.0.0
* 'debian/stretch64' for 'virtualbox' wasn't added from a catalog, no version in
formation
* 'centos/7' for 'virtualbox' is outdated! Current: 1811.02. Latest: 1902.01
----

Puc eliminar les versions antigues de les box que tinc instal·lades.
*Sempre i quant tingui més d'una versió de la mateixa màquina!!*.

[source]
----
D:\vagrantProjects>vagrant box prune -h
Usage: vagrant box prune [options]

Options:

    -p, --provider PROVIDER          The specific provider type for the boxes to
 destroy.
    -n, --dry-run                    Only print the boxes that would be removed.

        --name NAME                  The specific box name to check for outdated
 versions.
    -f, --force                      Destroy without confirmation even when box
is in use.
    -h, --help                       Print this help
----

Podem actualitzar el box corresponent *a un escenari*.

[source]
----
D:\vagrantProjects\escenari1> vagrant box update
----

////
=== Crear Box a partir d'una màquina virtual

Els box que surten d'aquest procés són grans!!

Mirem les màquines virtuals que tenim:

[source, cmd]
----
D:\Program Files\Oracle\VirtualBox>VBoxManage.exe list vms
"DC1" {1927b0cf-4d4a-41d5-8b2c-dea33ca02949}
"Docker Desktop" {55d5d582-1848-437d-8121-72ecafeabf8e}
"Docker Console" {bc925fea-ef49-4e50-b1ef-441e0e37d920}
"newbox_default_1556270992798_69222" {3e5d20f4-5995-4650-8c67-cf5bce383535}
----

Abans d'empaquetar caldrà tornar a posar la clau ssh pública per defecte a l'usuari vagrant, en cas contrari no podríem entrar per ssh a la nova box perquè perderiem la clau privada que s'ha generat aleatòriament per a la màquina virtual que estem empaquetant.

Les claus les tenim aquí:

https://github.com/hashicorp/vagrant

----
vagrant@contrib-stretch:~/.ssh$ sudo -i
root@contrib-stretch:~# cd /home/vagrant/
root@contrib-stretch:/home/vagrant# cd .ssh
root@contrib-stretch:/home/vagrant/.ssh# echo /vagrant/vagrant.pub
/vagrant/vagrant.pub
root@contrib-stretch:/home/vagrant/.ssh# cat /vagrant/vagrant.pub > authorize
d_keys
----

Empaquetem:

[source, cmd]
----
D:\vagrantProjects>vagrant package --base "newbox_default_1556270992798_69222" --output newbox.box
----

Podriem instal·lar el box amb:

[source, cmd]
----
D:\vagrantProjects>vagrant box add newbox.box --name newbox
----

Veiem que el nou box està disponible:

[source, cmd]
----
D:\vagrantProjects>vagrant box list
debian/contrib-stretch64 (virtualbox, 9.8.0)
flixtech/kubernetes      (virtualbox, 1.12.1)
newbox                   (virtualbox, 0)
ubuntu/trusty64          (virtualbox, 20190401.0.0)
----

Provem la nova box:

[source, cmd]
----
D:\vagrantProjects\tmp>vagrant init -m newbox
----
////

== Vagrantfile

El fitxer *_Vagrantfile_* defineix un *escenari de vagrant_*.

Un *escenari de vagrant* està format per un conjunt de màquines virtuals que en principi operen conjuntament.

=== Creació d'un Vagrantfile

Podem crear un fitxer de Vagrant amb la comanda:

[source]
----
vagrant init
----

En el cas d'un escenari amb una sola màquina virtual, podem especificar el box des del que es genera aquest a màquina amb:

[source]
----
vagrant init nom_box
----

Podem crear un *_Vagrantfile_* mínim amb la opció *_-m_*, per exemple:

[source]
----
vagrant init -m
----

=== Superposició de *_Vagrantfile_*

Existeixen més d'un Vagrantfile que es van superposant, explicat a :

https://www.vagrantup.com/docs/vagrantfile/#load-order-and-merging

De fet, els box de *_vagrant.d_* ja tenen un _Vagrantfile_.

=== Configuració del Vagrantfile

Creem una nova màquina:

[source]
----
D:\vagrantProjects\test>vagrant init -m debian/contrib-stretch64
----

.Vagrantfile
[source, ruby]
----
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.hostname = "node1"
  config.vm.define "vmnode1"
end
----

A partir d'aquí podem arrencar l'escenari amb:

[source]
----
vagrant up
----

Podem connectar-nos a la màquina virtual amb:

[source]
----
vagrant ssh
----

I parar l'escenari amb:

[source]
----
vagrant halt
----

=== Quants escenaris vagrant tenim?

La comanda *_vagrant global-status_* mostra tots els escenaris disponibles. Per exemple:

[source]
----
D:\>vagrant global-status
id       name       provider   state    directory
----------------------------------------------------------------------------
946191c  controller virtualbox poweroff D:/vagrantProjects/ansible
b53c9a5  node1      virtualbox poweroff D:/vagrantProjects/ansible
3b2b748  node2      virtualbox poweroff D:/vagrantProjects/ansible
44f22b9  default    virtualbox poweroff D:/vagrantProjects/kubernetes
ad5e691  dockerd    virtualbox poweroff D:/vagrantProjects/dockerd
151f984  dockerc    virtualbox poweroff D:/vagrantProjects/dockerd
2ba9835  base       virtualbox poweroff D:/vagrantProjects/debiand

The above shows information about all known Vagrant environments
on this machine. This data is cached and may not be completely
up-to-date (use "vagrant global-status --prune" to prune invalid
entries). To interact with any of the machines, you can go to that
directory and run Vagrant, or you can use the ID directly with
Vagrant commands from any directory. For example:
"vagrant destroy 1a2b3c4d"
----

=== Directori sincronitzat

Per defecte, el directori arrel de l'escenari al host està compartit amb totes les màquines del escenari.

Podem accedir-hi si anem a *_/Vagrant_* a la màquina virtual.

[NOTE]
====
En el cas que el "provider" sigui Virtual Box i la màquina virtual té instal·lades les "*Guest additions*" la compartició de carpetes serà automàtica.

*Si la màquina virtual no té els drivers* per permetre la compartició de carpetes amb el host es poden compartir mitjançant *NFS*, *rsync* o *SMB*.

No tots els Box baixats de Vagrant Cloud les inclouen, *cal mirar la descripció de cada box* per veure-ho.
====

Podem modificar la ruta de la carpeta compartida amb les màquines virtuals al fitxer *_Vagrantfile_* amb la directiva:

[source, ruby]
----
config.vm.synced_folder "src/", "/srv/website"
----

Per exemple:

[source, ruby]
----
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.hostname = "node1"
  config.vm.define "vmnode1"
  config.vm.synced_folder "shared1/", "/srv/shared", type: "smb", smb_username: "josep"
end
----

[NOTE]
====
La compartició amb *_rsync_* fa una sincronització *d'un únic sentit* des del host fins a la màquina virtual.

Per defecte aquesta sincronització es produeix només al fer *_vagrant up_* o *_vagrant reload_*, es pot habilita runa sincronització automàtica amb la comanda *_rsync-auto_*.
====

[NOTE]
====
Si el host és Windwos o MAC es poden compartir carpetes amb *_samba_*.
====

==== Paràmetres dependents del proveïdor

[WARNING]
====
Els paràmetres de *_Vagrantfile_* depenen del proveïdor, en aquest cas de *_Virtual Box_*.
====

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.hostname = "debian1"
  config.vm.define "debian1"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "debian1 - Vb"
    vb.memory = "512"
    vb.cpus = 2
  end
end
----

Podem obrir VirtualBox gràficament i veure que efectivament la màquina virtual està configurada correctament.

=== Passar paràmetres de VBoxManage

Si vagrant no suporta determinades comandes del proveïdor podem cridar al proveïdor directament des del fitxer *_Vagrantfile_*.

En el cas de Virtual Box podem afegir comandes de *_VboxManage_* al fitxer *_Vagrantfile_*. Per exemple:

Si volem crear un nou disc dur...

[source]
----
D:\Program Files\Oracle\VirtualBox>VBoxManage.exe createhd
Usage:

VBoxManage createmedium     [disk|dvd|floppy] --filename <filename>
                            [--size <megabytes>|--sizebyte <bytes>]
                            [--diffparent <uuid>|<filename>
                            [--format VDI|VMDK|VHD] (default: VDI)
                            [--variant Standard,Fixed,Split2G,Stream,ESX]

----

I volem afegir-lo a la màquina virtual...

[source]
----
D:\Program Files\Oracle\VirtualBox>VBoxManage.exe storageattach
Usage:

VBoxManage storageattach    <uuid|vmname>
                            --storagectl <name>
                            [--port <number>]
                            [--device <number>]
                            [--type dvddrive|hdd|fdd]
                            [--medium none|emptydrive|additions|
                                      <uuid|filename>|host:<drive>|iscsi]
                            [--mtype normal|writethrough|immutable|shareable|
                                     readonly|multiattach]
                            [--comment <text>]
                            [--setuuid <uuid>]
                            [--setparentuuid <uuid>]
                            [--passthrough on|off]
                            [--tempeject on|off]
                            [--nonrotational on|off]
                            [--discard on|off]
                            [--hotpluggable on|off]
                            [--bandwidthgroup <name>]
                            [--forceunmount]
                            [--server <name>|<ip>]
                            [--target <target>]
                            [--tport <port>]
                            [--lun <lun>]
                            [--encodedlun <lun>]
                            [--username <username>]
                            [--password <password>]
                            [--passwordfile <file>]
                            [--initiator <initiator>]
                            [--intnet]
----

Podríem utilitzar les comandes anteriors al següent *_Vagrantfile_*:

[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.hostname = "debian1"
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.provider "virtualbox" do |vb|
    vb.name = "debian1 - vb"
	vb.memory = "1024"
    vb.cpus = 2
	file_to_disk = 'tmp/disk.vdi'
	unless File.exist?(file_to_disk)
	    vb.customize ['createhd',
					 '--filename', file_to_disk,
					 '--size', 500 * 1024]
	end
    vb.customize ['storageattach', :id,
				   '--storagectl', 'SATA Controller',
				   '--port', 1,
				   '--device', 0,
				   '--type', 'hdd',
				   '--medium', file_to_disk]
	vb.customize ["modifyvm", :id, "--vram", "16"]
  end
end
----

=== Màquina virtual amb interfície gràfica

[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.hostname = "debian1"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "debian1 - vb"
    vb.memory = "512"
    vb.cpus = 2
	vb.gui = true
  end
end
----

=== Més paràmetres

Guardar només un diferencial del disc:

----
vb.linked_clone = true
----

=== Provisioning

Vagrant denomina aprovisionament (*_provisioning_*) als processos de configuració de la màquina virtual una vegada que aquesta ha finalitzat l'arrencada, processos que poden incloure l'execució de comandes de *_shell_*, l'execució d'un shell script complet o la utilització d'eines de la gestió de la configuració ("configuration management systems") com *_puppet_*, *_xef_* o *_ansible_*.

Aquest procés es realitza típicament *la primera vegada que s'inicia un entorn vagrant*, quan s'invoca directament des de la línia de comandes.

Es pot cridar explícitament amb la comanda *_vagrant up --provision_*.

==== Passar un script

.script.sh
[source, bash]
----
#!/bin/bash
apt update
DEBIAN_FRONTEND=noninteractive apt -y upgrade
----

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-stretch64"
  config.vm.provision "shell", path: "script.sh"
end
----

==== Passar paràmetres a un script

Podem parametritzar els scripts i passar paràmetres des del *_Vagrantfile_*, per exemple:

.bootstrap.sh
[source, bash]
----
#!/bin/sh

for arg in "$@"
do
	touch $arg
done
----

[NOTE]
====
Podríem fer referència a cadascun dels paràmetres amb *_$1_*, *_$2_*, etc...
====

.Vagrantfile
[source, ruby]
----
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-stretch64"
  config.vm.hostname = "argstest"
  config.vm.define "argstest"
  config.vm.provision "shell", path: "bootstrap.sh", args: "file1 file2 file3"
  config.vm.provider "virtualbox" do |vb|
	vb.name = "argstest"
    vb.memory = "512"
    vb.cpus = 1
  end
end
----

=== Redirecció de ports

Per defecte vagrant fa un NAT a la 10.0.1.0/24.

I per l'altra banda redirecciona el port 2222 al 22 per a poder fer un accés ssh, si aquest port ja està utilitzat n'utilitza un altre.

Imaginem que volem accedir des de fora de la màquina virtual a un servidor web.

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.hostname = "debian1"
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.provider "virtualbox" do |vb|
    vb.name = "debian1 - vb"
    vb.memory = "1024"
    vb.cpus = 2
  end
end
----

Per exemple.

====
Instal·lem servidor web

[source]
----
sudo apt update
sudo apt install apache2
----

I naveguem a la pàgina inicial des del host amb *_http://127.0.0.1:8080_*.
====

Podem saber els ports que estan redireccionats amb:

[source]
----
vagrant port
----

[source]
----
D:\vagrantProjects\test>vagrant port
The forwarded ports for the machine are listed below. Please note that
these values may differ from values configured in the Vagrantfile if the
provider supports automatic port collision detection and resolution.

    22 (guest) => 2222 (host)
    80 (guest) => 8080 (host)
----

Si fem modificacions a *_Vagrantfiles_* podem recarregar la configuració amb:

[source]
----
vagrant reload
----

=== Determinar la configuració ssh

La comanda *_vagrant ssh-config_* mostra la configuració ssh emprada per vagrant en l'escenari actual. Per exemple:

[source]
----
D:\vagrantProjects\test>vagrant ssh-config
Host default
  HostName 127.0.0.1
  User vagrant
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile D:/vagrantProjects/test/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
----

Aquesta configuració es pot modificar des del fitxer *_Vagrantfile_*

////
Podria accedir a la màquina amb:

Generar un clau putty amb *_puttygen_*
Putty:
Host Name 127.0.0.1
Port: 2222

auth: Afegir la clau creada al pas anterior

User: vagrant
----

----

O bé des de openssh:

ssh -p 2222 -i .vagrant/machines/host1/virtualbox/private_key vagrant@127.0.0.1
////

=== Snapshots

Primera opció

[source]
----
vagrant shanpshot save nom_snapshot
----

[source]
----
vagrant snapshot list
----

[source]
----
vagrant snapshot restore nom_snapshot
----

Segona opció

[source]
----
vagrant snapshot push
----

[source]
----
vagrant snapshot pop
----

=== Xarxes

Vagrant proporciona dos esquemes de xarxa

Xarxa privada::
** En el cas de Virtual Box equival a una xarxa  *_HostOnly_*
** Formada per:
*** Maquina virtual
*** IPv4 privada
*** Ip estàtica o dinàmica

+
Permet la connectivitat entre:
* Host i VM
* VM i VM

Xarxa pública::
En el cas de Virtual Box equival a una xarxa de tipus *_bridge_*.

[IMPORTANT]
====
Recordar que per defecte ja es proporciona una xarxa *_NAT_* automàticament.

Les xarxes privades o públiques creades s'afegeixen a la xarxa NAT existent.
====

==== Xarxa privada

Per exemple:

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.network "private_network", ip: "172.16.0.10"
end
----

Des del host puc fer

----
ping 172.16.0.10
----

i si miro les NIC del host :

[source]
----
Adaptador de Ethernet VirtualBox Host-Only Network #2:

   Sufijo DNS específico para la conexión. . :
   Vínculo: dirección IPv6 local. . . : fe80::f196:c959:a61c:5895%26
   Dirección IPv4. . . . . . . . . . . . . . : 172.16.0.1
   Máscara de subred . . . . . . . . . . . . : 255.255.255.0
   Puerta de enlace predeterminada . . . . . :
----

Si volem treballar amb DHCP:

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.network "private_network", type: "dhcp"
end
----

==== Xarxa pública

Per exemple:

.Vagrantfile
[source, ruby]
----
Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"
  config.vm.network "public_network", bridge: "Realtek PCIe GBE Family Controller"
end
----

=== Entorns multimàquina

Un escenari vagrant pot estar format per més d'una màquina:

[source, ruby]
----
Vagrant.configure("2") do |config|
    config.vm.define "web" do |node1|
        node1.vm.box = "debian/stretch64"
        node1.vm.hostname = "web"
        node1.vm.network "public_network", bridge: " Realtek PCIe GBE Family Controller"
        node1.vm.network "private_network", ip: "172.16.0.11"
     end
    config.vm.define "db" do |node2|
        node2.vm.box = "debian/stretch64"
        node2.vm.hostname = "db"
        node2.vm.network "private_network", ip: "172.16.0.12"
    end
end
----

En aquesta situació podem accedir a cada màquina proporcionant el nom definit a la clàusula *_define_*

[source]
----
vagrant ssh web
----
