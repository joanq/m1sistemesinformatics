:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

= Instal·lació desatesa de Debian 12

En aquesta pràctica preparem una imatge ISO que ens permeti fer una instal·lació
de Debian amb la mínima interacció possible.

Aquesta imatge ISO es podria gravar en un pendrive i es podria utilitzar sobre
qualsevol ordinador, però ho farem servir per a instal·lar sobre una màquina
virtual nova, perquè no volem esborrar el contingut actual dels discs durs dels
ordinadors de l'aula.

El procediment està documentat al link:https://www.debian.org/releases/stable/amd64/apb.en.html[manual d'instal·lació]
de Debian, i a la link:https://wiki.debian.org/DebianInstaller/Preseed[wiki].

La clau està en què l'instal·lador tingui un fitxer amb les respostes a les
preguntes que faria a l'usuari, i d'aquesta forma ja no li calgui preguntar-les.
Per proveir aquest fitxer a l'instal·lador hi ha diverses formes, per exemple,
es pot fer que es descarregui d'un servidor.

La forma més immediata i que no depèn de disposar de xarxa és incrustar el
fitxer de respostes dins de la pròpia ISO de l'instal·lador. Per això, agafarem
la ISO oficial, la convertirem a fitxers estàndard, modificarem/afegirem els
fitxers necessaris, i recrearem la imatge ISO.

[NOTE]
====
La pràctica s'ha de fer en el sistema operatiu Debian, preferentment en una
instal·lació nativa (no màquina virtual).
====

Per facilitar tot el procediment, utilitzarem les eines que hi ha al
repositori link:https://gitlab.com/joanq/insdebauto[Instal·lador automàtic de Debian 11].

. Utilitza `git` per clonar el repositori.
. Instal·la els prerequisits.
. Descarrega la imatge oficial de l'instal·lador. Guarda-la al directori
`insdebauto`.
. Observa l'script `init.sh`, s'hauria d'entendre pràcticament tot. Què fa
l'ordre `mount -o loop`? Què fa l'ordre `rsync`?
. Executa l'script `init.sh` passant-li de paràmetre el nom de la ISO de
l'instal·lador.
. El fitxer `preseed.cfg` és el fitxer amb les respostes que llegirà
l'instal·lador. Les línies començades amb # són comentaris. Abans d'afegir-lo a
la ISO, volem adaptar-lo a les nostres necessitats concretes:
.. Canvia la configuració de xarxa per tal que s'utilitzi DHCP per obtenir
l'adreça IP.
.. Assigna com a nom de host `debinst<nom>`, on `<nom>` és el teu nom, i *evita
que ho pregunti*. Per això, mira l'exemple inclòs al manual d'instal·lació de Debian.
.. Assigna a `root` la contrasenya `super3`.
.. Canvia el nom de l'usuari no administrador a `<nom>` (el teu nom). Posa també
`super3` com a contrasenya.
.. Fes que tots els fitxers vagin a una sola partició.
.. Afegeix a la llista de paquets a instal·lar el `gnome-terminal` i el `gparted`.

. Anem ara al fitxer `postinstall.sh`. Aquest fitxer és un script de bash
estàndard que s'executarà automàticament un cop feta la instal·lació (això
passa perquè se li ha indicat a les últimes línies de `preseed.cfg`).
.. Recorda que l'usuari `super` ara és `<nom>` (el teu nom).
.. Canvia l'usuari `alum-01` per `<cognom>` (el teu cognom). Posa-li contrasenya.
.. No creïs l'usuari `coord`.
.. A l'script hi ha les instruccions necessàries per instal·lar l'Atom i el
VirtualBox, que són programes que no són als repositoris oficials de Debian.
Treu la part que fa referència al VirtualBox. Afegeix les instruccions necessàries
per tal que s'instal·li automàticament el DBeaver.
.. Pel que fa a l'Atom, el programa s'ha deixat de desenvolupar. Modifica les
instruccions per tal que instal·li el Pulsar, un dels fork que s'ha creat d'Atom.

. Un cop modificats els fitxers, executa l'script `make_image.sh` per generar
la nova imatge ISO.
. Utilitza l'instal·lador generat per fer una instal·lació a una nova
màquina virtual. Quan apareix el menú del grub, selecciona opcions avançades, i
instal·lació automatitzada.
. Comprova que l'instal·lador no fa cap pregunta, que s'han creat els usuaris
que volíem, i que els programes addicionals estan instal·lats.
. Les últimes versions del VirtualBox incorporen una opció d'instal·lació desatesa.
Quina diferència hi ha entre fer la instal·lació desatesa que proposa el Virtualbox
i fer servir la imatge ISO que has generat en aquesta pràctica.
