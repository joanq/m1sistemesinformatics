== Discs durs

Un disc dur o disc rígid (en anglès Hard Disk, HD) és un *dispositiu d'emmagatzematge de dades no volàtil que empra un sistema d'enregistrament magnètic per emmagatzemar dades digitals*. 

Es compon d'un o més plats o discs rígids, units per un mateix eix que gira a gran velocitat dins d'una caixa metàl·lica segellada. Sobre cada plat se situa un capçal de lectura/escriptura que sura sobre una prima làmina d'aire generada per la rotació dels discs.

.Disc dir d'un IBM 305 RAMAC, primer ordinador comercial amb disc dur magnètic
image::images/ramac.jpg[]

El primer disc dur va ser inventat per IBM en 1956. Al llarg dels anys, els discs durs han disminuït el seu preu al mateix temps que han multiplicat la seva capacitat, sent la principal opció d'emmagatzematge secundari per PC des de la seva aparició en els anys 60. Els discs durs han mantingut la seva posició dominant gràcies als constants increments en la densitat d'enregistrament.

.Disc dur IBM 3340, primer model de disc dur amb factor de forma actual, 1973
image::images/ibm3340.jpg[]

Les grandàries també han variat molt, des dels primers discs IBM fins als formats estandarditzats actualment: 3,5" els models per PCs i servidors, 2,5" els models per a dispositius portàtils. Tots es comuniquen amb la computadora a través del *controlador de disc*, emprant una interfície estandarditzada. 

Les interfícies més comunes els darrers 20 anys són *IDE* (també anomenat ATA o PATA), *SCSI* (generalment usat en servidors i estacions de treball), Serial *ATA* i *SAS* (emprat exclusivament en servidors).

Per poder utilitzar un disc dur, un sistema operatiu ha d'aplicar un *format de baix nivell* que defineixi una o més *particions*. L'operació de formateig requereix l'ús d'una fracció de l'espai disponible en el disc, que dependrà del format emprat. 

[NOTE]
====
Els fabricants de discos durs, SSD i targetes flaix mesuren la capacitat dels mateixos usant prefixos SI, que empren múltiples de potències de 1000 segons la normativa IEC, en lloc dels prefixos binaris clàssics de la IEEE, que empren múltiples de potències de 1024, i són els usats majoritàriament pels sistemes operatius.

Això provoca que en alguns sistemes operatius sigui representat com a múltiples 1024 o com 1000, i per tant existeixin lleugers errors, per exemple un Disc dur de 500 GB en realitat és un disc dur de 465 GB.
====

El primer disc dur va ser desenvolupat per IBM en 1956. Va rebre el nom de RAMAC  i constava internament de 50 discos de 24” cadascun amb una capacitat total de 5 MB. No obstant això, el pare del disc dur modern va néixer en 1973, també de la mà d'IBM. El seu nom era 3340, i constava de dos mòduls de 30 MB, un de fix i l'altre extraïble. 

=== Estructura física d'un disc dur

.Vista interior d'un disc dur footnote:[Font: https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Seagate_ST33232A_hard_disk_inner_view.jpg/800px-Seagate_ST33232A_hard_disk_inner_view.jpg]
image::images/discDur.jpg[] 

Els elements físics que formen un disc dur són els següents:

Plats:: També anomenats discs. Aquests discs estan elaborats d'alumini o vidre recoberts en la seva superfície per un material ferromagnètic apilats al voltant d'un eix que gira gràcies a un motor, a una velocitat molt ràpida. El diàmetre dels plats oscil·la entre els 5cm i 13 cm.
Capçals:: Un per cada cara dels plats. Tots es desplacen al mateix temps. El braç es mou amb un electroimant. 
Motor que fa girar els plats:: Quant més alta és la velocitat de rotaci més petit és el temps d'accés.
Circuit electrònic de control:: Esta fora de la part hermètica que conté els plats i la resta de parts mecàniques. És la interfície de comunicacions amb l'ordinador i la memòria cau. També és l'encarregat de moure el braç dels capçals segons l'adreça de memòria que es vulgui llegir o escriure.
Bossa petita dessecant:: Gel de silici per evitar humitats.
Caixa metàl·lica i hermètica:: Protegeix el plats i els capçals de la brutícia. La part hermètica no est al buit: sovint té un petit forat amb un filtre que permet controlar la pressió de l'interior del disc (que es pot veure modificada per la calor i podria provocar que els capçals toquin els plats). 

=== Estructura lògica: Cares, Pistes i sectors

.Vista lògica d'un disc dur footnote:[Font: https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Cylinder_Head_Sector.svg/713px-Cylinder_Head_Sector.svg.png]
image::images/Cylinder_Head_Sector.svg.png[]

Cares:: Cada plat té dos cares. Cada cara té la seva corresponent pel·lícula magnètica.
Pistes:: Són unes circumferències concèntriques on és guarden les dades del disc. Estan enumerades i es comença a comptar per la pista exterior (Pista 0).
Sectors:: Les pistes estan dividides en arcs anomenats sectors. Totes les pistes tenen el mateix número de sectors numerats en una seqüència única per a tot el disc. La mida habitual d'un sector és de *512 bytes* per HDD i *2048 bytes* en CD-ROMs i DVD-ROMs.
+
Alguns dels discs durs més nous utilitzen sectors de 4KiB que es coneixen com sectors de format avançat.
+
Un sector és *la mínima unitat d'emmagatzematge d'un disc*. La majoria d'esquemes de particionat de disc estan dissenyats perquè els fitxers ocupin un nombre sencer de sectors independentment de la mida del fitxer, afegint zeros si cal a l'últim sector.

Capçals:: Els capçals són els transductors que llegeixen i/o escriuen la informació del disc. Cada cara té dos capçals (escriptura i lectura).
+
Els capçals mai toquen la superfície del disc i es mantenen a distàncies molt petites (nanometres) per l'efecte Air Bearing (coixinet d'aire); la velocitat de gir dels discs "enlaira" els capçals. Els capçals no poden tocar la superfície perquè la rallarien i provocarien danys fiscs. Els discs tenen una zona "d'aterratge" on col·locar els capçals un cop el disc deixa de girar (quan s'apaga).

Cilindres:: Els capçals van tots a l'hora sobre un braç motoritzat i es col·loquen sobre la mateixa posició en totes les cares. Les pistes de cada cara a les que s'accedeix de forma simultània són anomenades cilindres. 

////
==== Característiques d'un disc dur

En cada accés a la informació, el disc dur realitza un ampli conjunt de tasques. A continuació indiquem els passos fonamentals implicats en cada accés a disc:

. En primer lloc, es realitzen diverses etapes de traducció, que fan que una adreça (un nombre que apunta a una posició del disc) es tradueixi en una localització geomètrica, de tipus (*cilindre, capçal, sector*). 
. A continuació, es fa girar el disc, si és que aquest no estava ja en marxa (tots els discs durs moderns estan girant constantment). Quan el disc aconsegueix una velocitat estable, es mouen els capçals cap al cilindre desitjat, utilitzant el motor pas a pas que controla els braços.
. Una vegada en el cilindre apropiat, s'activa el capçal corresponent al plat desitjat. Llavors, s'espera el temps necessari perquè el gir del disc faci passar al sector desitjat sota el capçal. Quan això ocorre, es llegeix o s'escriu la informació en aquest sector.

El rendiment del disc dur està determinat per l'eficiència amb que es realitzen aquests passos.

El *temps d'accés* a la informació és la suma del temps emprat a portar els capçals cap al cilindre adequat (*temps mitjà de cerca*) i el temps de gir del disc fins a trobar el sector buscat (*latència*). El temps d'accés total sol oscil·lar entre 5 i 20 mil·lisegons.

Altres elements a tenir en compte en el funcionament de la unitat és el temps mitjà entre fallades, *MTBF* (Mean Time Between Failures), que es mesura en hores (15.000, 20.000, 30.000, etc.) i a major valor més fiabilitat del disc, ja que hi ha menor possibilitat de fallada de la unitat.

Un altre factor és el *AUTOPARK* o aparcament automàtic dels capçals, consisteix en el posicionament dels capçals en un lloc fora de l'abast de la superfície del disc dur de manera automàtic en apagar l'ordinador.

Existeixen una sèrie de Factors de Velocitat relacionats amb els discs durs que són necessaris conèixer per comprendre el seu funcionament i les seves diferències.

Temps de cerca de pista a pista:: Interval de temps necessari per desplaçar el capçal de lectura i escriptura des d'una pista a una altra adjacent.
Temps mitjà d'accés:: Temps que triga, com a mitjana, per desplaçar-se el capçal a la posició actual. Aquest temps mitjà per accedir a una pista arbitrària és equivalent al temps necessari per desplaçar-se sobre 1/3 de les pistes del disc dur. 
Latència Mitjana:: És la mitjana de temps perquè el disc una vegada a la pista correcta trobi el sector desitjat, és a dir el temps que triga el disc a donar mitja volta.
Velocitat de transferència:: velocitat a la qual les dades (bits) poden transferir-se des del disc a la unitat central. Depèn essencialment de dos factors: la velocitat de rotació i la densitat d'emmagatzematge de les dades en una pista.
Velocitat de Rotació:: Nombre de voltes per minut (RPM) que dóna el disc. Actualment entre 7200 i 20000 rpm

El treballar a velocitats elevades planteja diversos problemes en especial *la dissipació de la calor* ja que el disc, a aquestes velocitats, s'escalfa molt, i la vibració i el soroll del disc dur pot ser bastant alta.

A més de totes aquestes característiques de velocitats i temps d'accés dels discos durs existeixen una sèrie de tècniques que ens permeten minorar els accessos a disc així com accelerar les transferències de dades entre el sistema i el dispositiu en qüestió. Una de les tècniques més conegudes en la informàtica per fer això és la de l'ús de memòries intermèdies, buffer o caches.

Buffer De Pista:: És una memòria inclosa en l'electrònica de les unitats de disc, que emmagatzema el contingut d'una pista completa. Així quan es fa una petició de lectura d'una pista, aquesta es pot llegir d'una sola vegada, enviant la informació a la CPU.
Caches De Disc:: Avui dia estan dins del propi disc dur, anteriorment se situaven en targetes especials o bé a través de programes que usaven la memòria central. La gestió d'aquesta memòria és completament invisible i consisteix a emmagatzemar en ella les dades més demanades per la CPU. S'usen per descarregar al sistema de les lentes tasques d'escriptura en disc i augmentar la velocitat.
////

=== Discs durs d'estat sòlid

El “Estat Sòlid”, és un terme emprat per referir-se a components electrònics construïts enterament de semiconductors, i en particular, sense parts mòbils.

De fet, els SSD i la nostra típica memòria USB comparteixen moltes similituds, doncs els xips d'emmagatzematge que utilitzen són els mateixos o molt similars: la diferència està en la forma del disc (adaptada als actuals de 2.5” o 3.5” per poder cabre en les unitats existents, i en la capacitat de l'HD).

.Vista interior d'un disc ssd
image::images/ssdDisk.jpg[]

Els avantatges dels discs SSD són:

Consumeixen menys energia:: al no haver d'estar girant un disc extremadament ràpid, els discos durs SSD consumeixen menys energia. 
Major velocitat:: els SSD poden aconseguir una major velocitat, al no estar restringits a quantes revolucions poden rotar. En accés i lectura, la diferència és molt alta. 
Desapareix el soroll i la producció de calor:: Conseqüència de no haver-hi parts mòbils

Els desavantatges dels discs SSD són:

Tenen un període de vida menor al dels discs durs tradicionals:: El tipus de memòria utilitzada pels SSD té un nombre finit d'escriptures realitzables, igual que les memòries USB. Encara que contínuament aquest temps s'està millorant, ara com ara és millor un disc dur tradicional en aquest aspecte.
Preu elevat:: El preu per MB de SSD és més alt que el preu per MB en discos durs “normals”. Encara que el SSD està baixant de preu ràpidament, aquesta caiguda està actuant en SSD de poca capacitat.

==== Funcionament

L'element principal de la majoria de SSD s'anomena una cel·la NAND.

Per emmagatzemar un sol bit de dades en un SSD cal una d'aquestes cel·les, les mes simples poden emmagatzemar el valor d'un únic bit i mantenir aquest valor sense alimentació elèctrica.

.Cel·la NAND
image::images/nandcell.png[]

Una cel·la de memòria NAND emmagatzema la informació en el que s'anomena *transistor de porta flotant*, la càrrega elèctrica s'emmagatzema dins la porta flotant que està aïllada entre dues capes d'aïllant.

.Carregar una cel·la NAND
image::images/nand1.png[]

Per *carregar una cel·la* s'aplica voltatge elevat a la porta de control cosa que fa que es moguin electrons des del substrat de silici a la porta flotant.

.Esborrat d'una cel·la NAND
image::images/nand2.png[]

Per *esborrar una cel·la* s'aplica voltatge al substrat de silici cosa que inverteix el procés anterior buidant la porta flotant.

.Lectura d'una cel·la NAND (0)
image::images/nand3.png[]

.Lectura d'una cel·la NAND (1)
image::images/nand4.png[]

Per *llegir una cel·la* s'aplica voltatge a la porta de control i es fa passar corrent des de _Source_, si la corrent arriba a l'altra banda, a _Drain_, vol dir que no hi ha electrons a la porta flotant (1 binari) (el voltatge aplicat torna en conductor el substrat de silici permetent el flux de corrent), en cas contrari, la corrent no fluirà (0 binari)

En el procés d'escriptura i lectura de la cel·la es malmet la capa aïllant, amb el temps es trenca i és incapaç de mantenir carregada la porta flotant. això és el responsable de que els discs SSD vagin perdenrt capacitat amb el temps i s'hagin de retirar.

==== Tecnologies SLC, MLC i TLC

La cel·la explicada anteriorment és capaç de mantenir dos únics valors (carregada (0) o descarregada (1)), un disc SSD format amb aquestes cel·les es diu que és un disc SLC (*Single Level Cell*).

Les cel·les MLC (*Multi Level Cell*) permeten emmagatzemar diferents quantitats d'electrons a la porta flotant i mesurar la quantitat de corrent que flueix entre els dos extrems de la cel·la, d'aquesta manera una sola cel·la pot emmagatzemar més de dos valors.

Un disc MLC podrà emmagatzemar quatre valors per cel·la, és a dir 2 bits i un disc TLC podrà emmagatzemar 8 valors per cel·la (3 bits).

.Cel·la MLC
image::images/nand5.png[]

==== Limitacions de les memòries MLC i TLC

Per descomptat, afegir més bits en cada cel·la fa que sigui més difícil distingir entre els estats d'aquesta, la qual cosa redueix la fiabilitat, resistència i rendiment de la memòria. 

* Determinar si un "contenidor" està ple o buit (SLC) és molt més senzill que determinar si està a un "contenidor" complet, mig complet, a tres quarts de la seva capacitat, o completament ple (MLC). 
* A més, el temps de lectura i escriptura s'incrementa considerablement.
* Un altre efecte secundari d'emmagatzemar més bits per cada cel·la és l'increment del ràtio de degradació d'aquestes. 
* L'estat de cada cel·la NAND és determinat pel nombre d'electrons presents a la porta flotant. Les capes d'òxid que atrapen els electrons a la porta flotant es desgasten amb cada programació i esborrat de la cel·la. Amb un gran desgast, els electrons comencen a escapar més sovint. +
Això no era problema en les NAND SLC ja que l'estat era ple o buit. Però amb les cel·les multi estat la precisió és molt important, sobretot en les de 3 bit que necessiten diferenciar entre 8 estats. 

=== Interfícies

Per llegir i escriure correctament la informació en els suports d’emmagatzemament externs, és necessari un connector amb el qual unir el perifèric al bus del sistema. Se l’anomena *port de comunicacions* o controlador de disc. S’encarrega de:

* Administrar la transferència de dades
* Controlar les operacions d’entrada i sortida
* Establir connexió entre els perifèrics i el bus

Es troba instal·lat a la pròpia placa base o al propi disc dur.

Existeixen diversos estàndards principals d’interfície: IDE, SATA, SCSI, SATA, SAS 

Actualment tots els discs són *SATA* en les versions I, II o III utilitzats en entorns personals i *SAS* en servidors o ambients professionals.

El disc dur incorpora una placa de circuit imprès integrat o controladora, la seva funció és controlar tot el funcionament del disc. La circuiteria de la controladora incorpora un microprocessador i una memòria interna i cache per agilitzar les operacions de lectura i escriptura.

.Tipus d’interfícies
image::images/hddinterfaces.png[]

IDE (Parallel ATA (Advanced technology attachment))::
La seva característica més representativa era la integració de la controladora en el propi disc dur. Des d’aquest moment únicament es necessita una connexió entre el cable IDE i el bus del sistema, es possible integrar-la a la placa base o en una targeta. 
+
.Port IDE d'un disc dur
image::images/ideport.jpg[]

SATA (Serial ATA (Advanced technology attachment))::
És l'estàndard actual successor del IDE. Es passa del cable tradicional de 40 fils paral·lel, a un en sèrie que inclou 2 cables, un per transmissió i altre
per la recepció, a més a més d’uns altres 5 que fan servir el control de la transmissió. Una altra diferència és que es tracta d’una connexió punt a punt, cada disc dur ha d’anar connectat amb un cable serial ATA a la placa i ja no s’han de configurar com a Master-Slave. 

SCSI (Small computer System Interface):: 
La interfície SCSI va estar relegada a tasques i àmbits professionals, en els quals primava el rendiment, la flexibilitat i la fiabilitat.
+
SCSI era una estructura de bus independent del bus del sistema, així evitava les limitacions pròpies del bus de l’ordinador. A més a la seva versió més senzilla permetia connectar 7 dispositius a la vegada de tipus SCSI (scanner, impressora, CD, DVD).
+
Un altre avantatge era la portabilitat, això vol dir que podíem connectar el dispositiu SCSI en qualsevol altre tipus d’arquitectura (PC-MAC), sempre que fes servir l’estàndard SCSI. 
+
Actualment aquesta tecnologia ha estat substiutida per la tecnologia SAS, que proporciona totes les avantatges de SCSI però amb velocitats molt més altes, actualment les taxes de transferència son de 22.5GBits/s en la versió SAS4.
+
.Port SCSI d'un disc dur
image::images/scsiport.jpg[]

SAS (Serial Attached SCSI):: 
És una interfície de transferència de dades en sèrie que segueix utilitzant comandes SCSI per interaccionar amb els dispositius SAS.
+
Normalment els discs SAS treballen a 10.000/15.000rpm i estan reservats a entorns de servidor.
+
.Comparació entre una interfície sas i una interfície sata
image::images/sas.jpg[]

PCIe (Peripheal Component InterconnectExpress):: A mesura que evoluciona el mercat dels discs SSD queda patent que la velocitat de 6Gb/s d eles interfícies SATA és del tot inadequada pels dispositius d'emmagatzematge més ràpids.
+
PCIe, interfície utilitzada per connectar tot tipus de targes a la placa mare, multiplica per 20 la velocitat d'un bus sata i està esdevenint una interfície atractiva pels discs SSD d'última generació.
+
.Port PCIe d'un disc dur
image::images/pciediscport.png[]
