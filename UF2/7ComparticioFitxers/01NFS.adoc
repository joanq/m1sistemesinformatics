== Network File System

// https://graspingtech.com/mount-nfs-share-windows-10/

=== Introducció 

Es tracta d'un protocol, implementat el 1984 per l'empresa Sun Microsystems, que s'utilitza en xarxes d'àrea local.

Sun Microsystems va desenvolupar NFS com un estàndard obert i el va incloure un RFC perquè qualsevol pogués implementar-lo. footnote:[NFSv3 (RFC 1813), NFSv4 (RFC 3530)]

NFS (Network File System) és el sistema natiu utilitzat per Linux per compartir carpetes en una xarxa. Mitjançant NFS, un servidor pot compartir les seves carpetes a la xarxa. Des dels PCs dels usuaris es pot accedir a aquestes carpetes compartides i el resultat és el mateix que si estiguessin en el seu propi disc dur.

Bàsicament, NFS permet a PCs que utilitzen Linux, compartir i connectar-se a carpetes compartides entre si. 

Existeixen altres alternatives per compartir carpetes en una xarxa com **samba**, **ssh** o **ftp**, però el **sistema recomanat** per compartir carpetes entre sistemes Linux és NFS.

=== Arquitectura

El protocol NFS segueix un un model client-servidor, el *servidor* proporciona un sistema de fitxers local que pot ser muntat per un client situat en una ubicació remota de la xarxa.

* L' equip que actua com a servidor emmagatzema els arxius compartits.
* Els equips que actuen com a clients, accediran als arxius compartits pel servidor com si fossin locals.

=== Clients

Actualment, el protocol NFS està inclòs en la *majoria de les distribucions Linux*, i en les diferents versions de l’*OS d'Apple*. 

A Microsoft:

* Abans de Windwos 8 calia instal·lar el paquet Windows Services for UNIX.
* A partir de Windows 8 ja s'inclou de fàbrica, però només en la versió Enterprise edition. Una solució per salvar les incompatibilitats és recórrer a programari de tercers.
* A Windows 10 inicialment només s'incloïa a la versió Enterprise però actualment s'inclou a totes les versions.

=== Escenaris

Tenint en compte això, hi ha diversos usos típics de l’NFS en què aquest servei mostra la utilitat que té:

* Centralització dels directoris *_home_* dels usuaris.
* Compartició de directoris d’ús comú.
* Situar programari en un sol ordinador de la xarxa.
+
És possible instal·lar programari en un directori del servidor NFS i compartir aquest directori via NFS. Si configurem els clients NFS perquè muntin aquest directori remot en un directori local, aquest programari estarà disponible per a tots els ordinadors de la xarxa.
* Compartició per mitjà de la xarxa de dispositius d’emmagatzematge. (Particions, discs durs, unitats extraïble)

Totes les operacions sobre fitxers són *síncrones*. Això significa que l’operació només torna un resultat quan el servidor ha completat tot el treball associat a aquesta operació. 

Per exemple, en cas d’una sol·licitud d’escriptura, el servidor escriurà físicament les dades en el disc i, si és necessari, actualitzarà l’estructura de directoris abans de tornar una resposta al client. *Això garanteix la integritat dels arxius*.

////
Funcionament de NFS

En el sistema client, el funcionament de l’NFS es basa en la capacitat de traduir els accessos de les aplicacions a un sistema d’arxius en peticions al servidor corresponent per mitjà de la xarxa. 

Normalment aquesta funcionalitat del client està programada en el nucli de GNU/Linux, de manera que no necessita cap tipusde conﬁguració.

Respecte al servidor, l’NFS s’implementa mitjançant dos serveis de xarxa:

• El servei mountd s’encarrega d’atendre les peticions remotes de muntatge, efectuades per l’ordre mount del client. Entre altres coses, aquest servei s’encarrega de comprovar si la petició de muntatge és vàlida i de controlar sota quines condicions s’accedirà al directori exportat (només lectura, lectura/escriptura, etc.). 

Una petició es considera vàlida quan el directori sol·licitat ha estat explícitament exportat i el client té prou permisos per muntar aquest directori.

• El servei nfsd s’encarrega de, un cop s’ha muntat un directori remot correctament, atendre i resoldre les peticions d’accés del client als arxius que hi ha en el directori

Instalació del servei NFS

Per a poder usar el servei de compartir carpetes a la xarxa mitjançant NFS, al servidor és necessari instal·lar el paquet NFS de servidor.
Per instal·lar el servei en el servidor, farem:
// Instalació de NFS

# apt-get install nfs-kernel-server 

El paquet nfs-kernel-server depèn del paquet nfs-common. Per tant, tots dos s’instal·laran conjuntament amb l’ordre anterior. (Nota: això pot variar en futures versions de Debian i derivats, com Ubuntu)

Configuració del servei NFS
Abans d'engegar el servei NFS, es necesssari indicar quines carpetes desitgem compartir i si volem que els usuaris hi puguin accedir amb permisos de només lectura o de lectura i escriptura.
També existeix la possibilitat de “filtrar” quins clients volem que hi tinguin accés i quins no.
Totes aquestes opcions es configuren a l'arxiu /etc/exports
A cada línia de l'arxiu de configuració del servei NFS /etc/exports, es pot especificar:

La carpeta que es vol compartir
El mode en què es comparteix (només lectura 'ro' o lectura i escriptura 'rw' )
Des de quin PC o PC's es permet l'accés a la carpeta ( podem filtrar per nom de host, per IP o filtrar tot un rang d'IP's)
A continuació podeu veure un senzill arxiu d'exemple /etc/exports per configurar algunes capretes compartides:
// Exemple d'arxiu /etc/exports de configuració del servei NFS:
# Compartir la carpeta home del servidor
#  en mode lectura i escriptura. A més, accessible des de la xarxa 192.168.0.0/24

/home   172.16.15.0/255.255.255.0(rw)

# Compartir carpeta tmp a tothom como 'només lectura'
/tmp    *(ro)

# Compartir carpeta /var/log a un PC com 'només lectura'
/var/log 172.16.15.121(ro)
Les opcions de muntatge més importants que es poden especiﬁcar entre parèntesi. Per a cada ordinador o grup són les següents:

• ( ): Aquesta opció estableix les opcions que l’NFS assumeix per defecte.

• ro: El directori s’exporta com un sistema d’arxius només de lectura 

• rw: El directori s’exporta com un sistema d’arxius de lectura/escriptura.
• root_squash: Els accessos des del client amb UID = 0 (root) es converteixen en el servidor en accessos amb el UID d’un usuari anònim.

• no_ root_squash: Es permet l’accés des d’un UID = 0 sense conversió. És a dir, els accessos d’arrel (root) en el client es converteixen en accessos d’arrel en el servidor.

• all_squash: Tots els accessos des del client, amb qualsevol UID, es transformen en accessos d’usuari anònim

• anonuid, anongid: quan s’activa l’opció root_squash o allsquash, els accessos anònims utilitzen normalment l’UID i el GID primari de l’usuari  denominat nobody si aquest usuari existeix en el servidor. Si es volen utilitzar altres formes d’identiﬁcació, els paràmetres anonuid i anongid estableixen, respectivament, quins UID i GID tindrà el compte anònim que el servidor utilitzarà per accedir al contingut del directori.

• noaccess: impedeix l’accés al directori especificat. Aquesta opció és útil per impedir que s’accedeixi a un subdirectori d’un directori exportat.
Nota: Els permisos de compartició per NFS no exclouen als permisos del sistema unix sinó que prevalen els més restrictius. Si una carpeta està compartida amb permís NFS de lectura i escriptura però en els permisos del sistema només disposem de permís de lectura, no podrem escriure. Si una carpeta està compartida amb permisos NFS de lectura i disposem de permisos de lectura i escriptura en el sistema, tampoc podrem escriure. Per poder escriure necessitarem disposar permís de lectura i escriptura tant en els permisos del sistema com en els permisos de compartició NFS. Igualment, si compartim la carpeta /home amb permisos de lectura i escriptura però l'usuari pepe només té accés a la carpeta /home/pepe, no podrà accedir a cap altra carpeta dins de /home que els permisos del sistema l'hi impedeixin.
Quan es comparteix per NFS, es recomana restringir al màxim els permisos. Si els usuaris no tenen la necessitat d'escriure, hem de compartir amb permís de 'només lectura'. Si els usuaris només es connecten des de la nostra xarxa 172.16.15.0/24, hem de permetre l'accés només des d'aquesta xarxa.

Engegada i aturada manual del servei NFS
Perquè el servidor NFS funcioni, cal que estigui engegat el servei, per tant, la primera acció serà iniciar portmap per si no estigués engegat:
// Iniciar portmap
# /etc/init.d/portmap start
Per posar en marxa el servei NFS, o cada vegada que modifiquem l'arxiu /etc/exports, hem reiniciar el servidor NFS, mitjançant la comanda: 
// Reinicio del servei NFS
# /etc/init.d/nfs-kernel-server restart
Si volem aturar el servei NFS, hem d'executar:
// Parada del servei NFS
# /etc/init.d/nfs-kernel-server stop              


Engegada automàtica de NFS a l'inici del sistema


Atenció! Aquest punt, com ja sabeu, depén del tipus d'inici de sistema que tinguem al nostre Linux, ja sigui SystemV o systemd (entre d'altres)

Per a SystemV, disposem d'aquestes comandes:

# update-rc.d nfs-kernel-server defaults

Per a systemd, tenim la comanda systemctrl:

# systemctrl enable nfs
Configuració del client per a poder accedir a les carpetes compartides.

Hem d'assegurar-nos que tenim els paquets necessaris per connectar com a clients del sistema NFS, al client executarem:
# apt-get install nfs-common

Per poder accedir des d'un PC a una carpeta compartida per NFS en un servidor, cal muntar la carpeta compartida en el nostre sistema de fitxers. D'aquesta manera, l'accés a la carpeta compartida és exactament igual que l'accés a qualsevol altra carpeta del nostre disc dur.
Exemple, suposem que un servidor comparteix per NFS la carpeta anomenada / home. Al PC client podem crear una carpeta anomenada / mnt / home-servidor i muntar-hi la carpeta compartida al servidor. Per això, en el client i com a root executaríem la següent comanda:
//Mostrar les carpetes exportades pel servidor NFS
 # showmount -e ip-del-servidor

// Muntar carpeta compartida per NFS
# mount -t nfs ip-del-servidor:/home /mnt/home-servidor
A partir d'aquest moment, podem comprovar que la nostra carpeta / home-servidor conté la informació de la carpeta / home del servidor.
Si disposem de permisos de lectura i escriptura, podem fins i tot crear o modificar els arxius dins la nostra carpeta / home-servidor i els canvis s'estaran guardant realment a la carpeta / home del servidor.




Captura de pantalla després d'executar les comandes showmount i df
Per realitzar el muntatge, hem de fer sobre una carpeta existent en el nostre sistema. Si aquesta carpeta del nostre sistema conté arxius, aquests no estaran accessibles ja que la carpeta ens mostrarà els arxius remots.
Si volem que el nostre PC muntanya sempre de manera automàtica una carpeta compartida per NFS quan iniciem el nostre Linux, hi ha la possibilitat d'afegir a l'arxiu /etc/fstab una línia com per exemple:
ip-del-servidor:/home /mnt/home-servidor nfs

D'aquesta manera, quan arrenquem el nostre PC, la carpeta / home del servidor quedarà automàticament muntada sobre la nostra carpeta / mnt / home-servidor i no haurem de executar la comanda mount per res.
Si volem tornar a carregar o reiniciar el fitxer fstab sense reiniciar l'ordinador, només executarem:
#Tornar a carregar el /etc/fstab sense reiniciar:
# mount -a


REFERÈNCIES

“Administració de l’accés al domini” (Material IOC)

“Instal·lar i configurar NFS: Carpetes compartides a Ubuntu” (http://tecnoloxiaxa.blogspot.com)

“Network File System” (Ubuntu Server Guide)
////