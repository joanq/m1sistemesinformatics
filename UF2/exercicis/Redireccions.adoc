:doctype: article
:encoding: utf-8
:lang: ca
:ascii-ids:

= LineaOrdres - Redireccions

== Exercicis

Podeu trobar referències sobre Redireccions als apunts:

- link:../2LiniaOrdres/09ioredirectionLinux.adoc[UF2 - 02 LineaOrdres - 09ioredirectionLinux]
- link:../2LiniaOrdres/10ioredirection2Linux.adoc[UF2 - 02 LineaOrdres - 10ioredirection2Linux]

=== Redireccions

1.	Crea la `carpeta1`.
2.	Crea un fitxer que es digui `octubre_2009` i que contingui el calendari del mes
d'octubre de 2009.
3.	Canvia el nom del fitxer i posa-li, `octubre_novembre_2009`.
4.	Afegeix el calendari de novembre 2009 al fitxer i comprova el resultat.
5.	Executa la següent comanda `cat f1` i redirigeix la sortida d'error a un
fitxer anomenat `error.txt`.
6.	Copia el contingut de tots els fitxers que acabin en `.log` del directori
`/var/log` al fitxer `error.txt`.	Comprova que s'han copiat.
7. Copia el nom de tots els fitxers que acabin en `.log` del directori
`/var/log` al fitxer `llista_error.txt`. Comprova el resultat.
8.  Crea un fitxer que es digui `usuarisigrups` que contingui tot el contingut
dels fitxers `/etc/passwd` i `/etc/group`.
9. Crea un fitxer que es digui `usus_vip` que contingui els 5 últims usuaris del
fitxer `/etc/passwd`.
10. Utilitzant la comanda `tr`, canvia el caràcter `a` per una `x` de tot el
fitxer `usus_vip`. Utilitza la redirecció d'entrada.
11. Fes la mateixa comanda, però deixant el resultat en un fitxer anomenat
`usus_encriptat`. 
12. Fes una exercici que demani diversos noms a través d'una redirecció doble
d'entrada amb un indicador final que posi `FIN`. Hauria d'indicar el nombre de
noms introduïts.
