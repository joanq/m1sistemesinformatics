== Classificació de les xarxes

Diferents classificacions possibles:

* Segons la titularitat.
* Segons l’extensió.
* Segons la topologia.
* Segons la relació funcional entre els diferents nodes.
* Segons la transmissió de la informació.

=== Classificació segons la titularitat

Aquesta classificació té en compte la propietat de la xarxa.

((Xarxes privades)) o dedicades::
Les línies de comunicacions de les xarxes dedicades són dissenyades i
instal·lades per l’usuari o administrador del sistema, o bé llogades a les companyies de comunicacions que es dediquen a oferir aquests serveis, sempre per l'ús exclusiu de qui les lloga.

((Xarxes públiques)) o compartides::
En aquestes xarxes les línies de comunicació suporten informació de
diferents usuaris. Es tracta de xarxes de servei públic ofertes per companyies de telecomunicacions, per les quals s’ha de pagar una quota depenent de la utilització que se’n fa. Un exemple és la xarxa de telefonia fixa, la xarxa de telefonia mòbil, XDSI, ADSL, xarxes de fibra òptica, etc.

.Xarxes privades virtuals
****
Un cas freqüent de xarxa dedicada és el que s’anomena *((Xarxa Privada Virtual))* (*((VPN))* – Virtual Private Network)

* Una xarxa privada virtual és una xarxa privada que s'estén a través d'una xarxa pública o Internet. 
* Permet als usuaris rebre i enviar dades a través de xarxes públiques com si es tractés d'una xarxa privada amb tota la seva funcionalitat. 
* Tot això s'aconsegueix configurant determinats nodes de la xarxa pública perquè redirigeixin el tràfic de la vpn dins de les fronteres de la pròpia vpn.
* Amb  una VPN una persona pot accedir a la xarxa de l’empresa a través d’Internet, realitzant una "tunelització" segura entre el seu ordinador personal i un encaminador VPN situat a la seu de l’empresa; l’ordinador personal pot estar ubicat en qualsevol lloc amb connexió a Internet, com  per exemple l’oficina de treball, la residència habitual, la segona residència, un hotel, etc.
****

=== Classificació segons l’extensió

Està clar que és un factor a tenir en compte a l’hora de dissenyar i muntar una xarxa. *No obstant aquesta classificació és confusa i arbitraria.*

==== ((Xarxes d’àrea personal)) 

També anomenades Personal Area Network, Wireless personal Area Network o amb les seves sigles ((PAN)) i ((WPAN))) respectivament.

Són xarxes de computadors  orientades a la interconnexió de diferents dispositius propers al punt d’accés.

Aquestes xarxes normalment són d’ús personal i tenen un abast de pocs metres. 

Algunes característiques importants::
** La configuració d’accés a la xarxa sol ser molt simple o automàtica.
** El radi d’acció de la xarxa ha de ser geogràficament molt limitat amb l’objectiu que dues xarxes diferents no col·lisionin entre elles.
** El medi de transmissió sol ser sense fils.
** Els costos d'instal·lació i d’explotació de la xarxa  han de ser petits o zero.

Exemples::
** Ratolins sense fils, impressores per infrarojos, bluetooth, etc... 

==== ((Xarxa d’àrea local))

També anomenades Local Area Network o amb les seves sigles ((LAN))

Són xarxes de propietat privada que es troben en un sol edifici o en un campus de pocs quilometres de longitud.

S’utilitzen per connectar PC, impressores i estacions de treball en oficines d’una empresa, etc...

Les LAN són es diferencien d’altres tipus de xarxes en tres aspectes::
. Mida
. Tecnologia de transmissió (tipus de cables específics, possibilitat de difusió sense massa impacte)
. Topologia (pe. bus, anell) 

Característiques de les xarxes locals::
** Cometen pocs errors
** Velocitats entre 10 a 1000 Mbps (actualment fins a 10000Mbps)
** Temps de transmissió conegut  i limitat.
** Tot això permet utilitzar topologies i tècniques que no són factibles en altres tipus de xarxes. 

.Exemple xarxa LAN
image::images/xarxaLan.png[]

==== ((Xarxa de campus))

Són xarxes que s’estenen entre varis edificis dintre d’un mateix polígon.

Normalment l’*empresa propietària del terreny* té llibertat per cablejar-lo com vulgui. 

.Exemple xarxa campus
image::images/xarxaCampus.png[]

==== ((Xarxa d’àrea metropolitana)) 

També anomenades Metropolitan Area Network o per les seves sigles ((MAN))

Són xarxes que abasten una ciutat.

Característiques::
** Estan subjectes a regulacions locals.
** Poden constar de recursos públics i privats
** Estan dissenyades per proporcionar als seus usuaris la distribució de dades interconnectant les diferents LAN. 
 
Per exemple una xarxa de televisió per cable podria ser una xarxa MAN.

==== ((Xarxa d’àrea extensa)) 

També anomenades Wide Area Network o per les seves sigles ((WAN))

Abasten una gran àrea geogràfica, un país o un continent.

Característiques::
** Els enllaços WAN els gestionen empreses de telecomunicacions publiques o privades.
** El medi de transmissió sol ser microones, fibra o via satèl·lit. 
** Les transmissions en una WAN es realitzen a través de línies públiques.
** La capacitat de transmissió sol ser menor que les utilitzades en els LAN.
** Solen estar compartides entre molts usuaris a la vegada i per tant fan falta normes que regulin la interconnexió a la xarxa.
** La taxa d’error d’una WAN és major que la d’una LAN

.Exemple xarxa WAN
image::images/xarxaWan.png[]

=== Altres xarxes relacionades amb l'extensió

Actualment també es parla de ((comunicació *NFC*)) (Near Field Comunication)

* Conjunt de protocols que permet a diferents dispositius electrònics comunicar-se entre ells a través del contacte o per proximitat (uns 10cm màxim)

També s'esta parlant de les *Xarxes d'àrea corporal* (BAN – Body Area Network)

* Xarxes sense fils muntades per mitjà d'implants. 

=== Xarxes especialitzades 

Existeixen algunes xarxes d'ús específic que val la pena conèixer.


==== Subxarxa o ((segment de xarxa))

* Conjunt d’estacions que comparteixen el mateix medi de transmissió (pe. el mateix cable).
** No fan falta dispositius commutadors.
** És la xarxa de comunicació més petita, les xarxes més grans es formen unint segments de xarxa. 

==== ((Xarxa troncal))

També anomenades xarxes ((backbone)).

Consisteixen en la part d'una xarxa d'ordinadors que interconnecta diferents xarxes entre elles, sovint LANs.

* Una xarxa troncal pot interconnectar diverses xarxes en el mateix edifici, en edificis diferents o en entorns WAN. 
* Per exemple, el tram que ajunta dues LANs en edificis diferents dins d'un polígon industrial formant una xarxa de campus és una troncal.

////
==== Xarxa ((SAN))

O Storage area network.

Una xarxa d’àrea d’emmagatzematge (SAN) és una xarxa d’alt rendiment dedicada a tasques molt concretes, com moure dades entre servidors i oferir recursos d’emmagatzematge. Aquest tipus de xarxes SAN s’instal·len fora de la LAN per evitar el trànsit que ocasionen les connexions entre clients i servidors. 

Les SAN tenen les característiques següents::
* S'acostumen a trobar dins d'una LAN.
* Són xarxes d'*alt rendiment*
** Permeten l’accés concurrent de matrius de disc o cinta per dos o més servidors a
 alta velocitat, proporcionant un millor rendiment del sistema.
* Tenen alta *disponibilitat*
 ** Tenen una tolerància incorporada als desastres, ja que es pot fer una còpia
exacta de les dades mitjançant una SAN fins a una distància de10 km o6,2 milles.
* Permeten una bona *escalabilitat*
** De la mateixa manera que una LAN/WAN, pot usar una gamma àmplia de tecnologies. Això
permet una reubicació fàcil de dades de còpia de seguretat, operacions, migració d’arxius, i duplicació de dades entre sistemes. 
////

////
=== Classificació segons la relació funcional

* Aquesta classificació té en compte la distribució dels diferents serveis de xarxa en els diferents nodes de la xarxa.

==== Model mainframe

Un mainframe és una computadora gran, potent i costosa usada principalment  per una gran companyia pel processament d'una gran quantitat de dades;per exemple, pel processament de transaccions bancàries.

* El mainframe és l'únic ordinador de la xarxa i per tant gestiona tots els serveis de la mateixa.
* L'accés al mainframe es realitza mitjançant terminals, dispositius dotats
essencialment d'un teclat, una pantalla i una tarja de xarxa. 
* És un model actualment en desús degut principalment a l'elevat cost tant del maquinari com del programari.
* Tècnicament aquest model no configura una xarxa donat que només hi ha  un ordinador. 

==== Model Client-Servidor

En el model client-servidor les tasques es reparteixen entre els proveïdors de recursos o serveis, anomenats *servidors*, i els demandants, anomenats *clients*.

* Un *client* realitza peticions a un altre ordinador, el servidor, que li dóna resposta.
* En aquesta arquitectura la capacitat de procés està repartida entre els clients i els servidors, encara que són més importants els avantatges de tipus organitzatiu degudes a la centralització de la gestió de la informació i la separació de responsabilitats, la qual cosa facilita i aclareix el disseny del sistema.
* Actualment és un model molt utilitzat. 

Avantatges::
Centralització del control:::
Els accessos, recursos i la integritat de les dades són controlats pel
servidor de manera que un programa client defectuós o no autoritzat no pugui danyar el sistema. Aquesta centralització també facilita la tasca de posar al dia dades o altres recursos (millor que a les xarxes P2P)...
Escalabilitat:::
Es pot augmentar la capacitat de clients i servidors per separat. Qualsevol element pot ser augmentat (o millorat) en qualsevol moment, o es poden afegir nous nodes a la xarxa (clients i/o servidors).
Fàcil manteniment:::
En estar distribuïdes les funcions i responsabilitats entre diversos ordinadors independents, és possible reemplaçar, reparar, actualitzar, o fins i tot traslladar un servidor, mentre que els seus clients no es veuran afectats per aquest canvi (o s'afectaran mínimament). 

Desavantatges::
La congestió del tràfic:::
Quan una gran quantitat de clients envien peticions simultànies al mateix servidor poden col·lapsar la xarxa. (A les xarxes P2P com cada node a la xarxa fa també de servidor, com més nodes hi ha, millor és l'ample de banda que es té.)
Poca robustesa:::
Quan un servidor està caigut, les peticions dels clients no poden ser satisfetes. (En la major part de xarxes P2P, els recursos estan generalment distribuïts en diversos nodes de la xarxa. Encara que alguns surtin o abandonin la descàrrega; uns altres poden encara acabar de descarregar aconseguint dades de la resta dels nodes a la xarxa.)
Cost:::
El programari i el maquinari d'un servidor són generalment molt específics. Les versions especifiques per a servidors tant de maquinari com de programari son molt mes cares que les versions client. 

==== Model peer-to-peer

Una xarxa Peer-to-Peer o xarxa de parells o xarxa entre iguals (P2P, per les seves sigles en anglès) és una xarxa de computadores en la qual tots o alguns aspectes funcionen sense clients ni servidors fixos, sinó una sèrie de nodes que es comporten com a iguals entre si.És a dir, actuen simultàniament com a clients i servidors respecte als altres nodes de la xarxa. 

* Les xarxes peer-to-peer aprofiten, administren i optimitzen l'ús de l'ample de banda dels altres usuaris de la xarxa per mitjà de la connectivitat entre els mateixos, i obtenen així més rendiment en les connexions i transferències que amb alguns mètodes centralitzats convencionals, on una quantitat relativament petita de servidors proveeix el total de l'ample de banda i recursos compartits per a un servei o aplicació. 
* Aquestes xarxes són útils per a diversos propòsits. Sovint s'usen per compartir
fitxers de qualsevol tipus (per exemple, àudio, vídeo o programari).Aquest tipus de xarxa també sol usar-se en telefonia VoIP per fer més eficient la transmissió de dades en temps real. 

Un exemple molt comú són les xarxes que treballen en un únic grup de treball de Windows.

Característiques::
Robustesa:::
La naturalesa distribuïda de les xarxes peer-to-peer incrementa la robustesa en cas d'haver-hi fallades ja que al ser tots els nodes“iguals” si en falla un qualsevol altre el pot substituir.
Descentralització:::
Aquestes xarxes per definició són descentralitzades i tots els nodes són iguals. No existeixen nodes amb funcions especials, i per tant cap node és imprescindible per al funcionament de la xarxa. (En realitat, algunes xarxes comunament anomenades P2P no compleixen aquesta característica,com Napster, eDonkey o BitTorrent.)
Distribució de costos entre els usuaris:::
Es comparteixen recursos a canvi de recursos. Segons l'aplicació de la xarxa, els recursos poden ser arxius, ample de banda, cicles de procés o emmagatzematge de disc.. 
Anonimat:::
És desitjable que en aquestes xarxes quedi anònim l'autor d'un contingut, l'editor, el lector, el servidor que ho alberga i la petició per trobar-ho, sempre que així ho necessitin els usuaris. Moltes vegades el dret a l'anonimat i els drets d'autor són incompatibles entre si, i la indústria proposa mecanismes com el DRM per limitar tots dos.
Poca seguretat:::
Els objectius d'un P2P segur serien identificar i evitar els nodes maliciosos, evitar el contingut infectat, evitar l'espionatge de les comunicacions entre nodes, creació de grups segurs de nodes dins de la xarxa, protecció dels recursos de la xarxa... 

==== Model thin-client

Un client lleuger (thin client o slim client en anglès) és una computadora client en una xarxa client-servidor que depèn primàriament del servidor central per a les tasques de processament, i principalment s'enfoca a transportar l'entrada i la sortida entre l'usuari i el servidor remot. 

* Els dispositius de client lleuger poden tenir solament navegadors web o
programes d'escriptori remot, o poden córrer sistemes operatius complets, qualificant-los com a nodes sense disc o clients híbrids. 
* No obstant a diferencia de les terminals clàssiques, els thin clients tenen certa potència de càlcul.

.Thin client
image::images\xarxesthinclient.jpg[]

==== Model cloud

En aquest tipus de computació tot el que pot oferir un sistema informàtic s'ofereix com a servei de manera que els usuaris puguin accedir als serveis disponibles "en el núvol d'Internet"  sense coneixements (o,almenys sense ser experts) en la gestió dels recursos que usen.

* Segons el IEEE Computer Society, és un paradigma en el qual la informació
s'emmagatzema de manera permanent en servidors d'Internet i s'envia a cachés temporals de client, la qual cosa inclou equips d'escriptori, centres d'oci, portàtils, etc. 
////

=== Classificació de les xarxes segons la seva topologia

Aquesta classificació es basa en la topologia física de la xarxa, és a dir, com s’interconnecten els elements de la xarxa. 

==== ((Topologia física))

Es pot definir la *topologia física* d'una xarxa com la cadena de comunicació que el nodes que conformen una xarxa utilitzen per a comunicar-se.

* La topologia d’una xarxa determina únicament la configuració de les connexions entre nodes.
+
[IMPORTANT]
====
La distància entre els nodes, les interconnexions físiques, les taxes de
transmissió i/o els tipus de senyals NO pertanyen a la topologia de xarxa tot i que es poden veure afectats per la mateixa.
====
* En general ens referim a la topologia d’una xarxa a seques quan fem referència a la seva topologia física. 


.Exemple de topologia
image::images/topologia1.png[]

.Exemple de topologia
image::images/topologia2.png[]

.Exemple de topologia
image::images/topologia3.png[]

==== Topologia d’estrella

.Topologia d'estrella
image::images/topologiaestrella.png[600, 600]

Els equips es connecten a un node central amb funcions de commutació i control.

Normalment el node central no funciona com estació, mes aviat acostuma ser un dispositiu específic. (un hub o concentrador)

Avantatges::
* Si un segment es trenca la xarxa segueix funcionant.
* El cablejat és relativament estructurat tot i que el cablejat al concentrador pot ser embolicat.
* Econòmica
* Fàcil administració

Inconvenients::
* Si el node central falla es paralitza tota la xarxa.
* Rendiment cau al afegir-hi nodes
* Nombre limitat de nodes

Xarxes que implementen la topologia::
* Ethernet

==== Topologia de bus

.Topologia en bus
image::images/topologiabus.png[600, 600]

Utilitza un únic cable per connectar el equips.

En el moment en que una estació hi posa una trama totes les altres estacions la recullen i miren si en són el destinatari, si es així se la queden, si no la descarten.

Avantatges::
* Necessita molt poc cable i per tant econòmica

Inconvenients::
* Si falla algun enllaç tots els nodes queden aïllats.
* Difícil de localitzar en quin punt hi ha la falla. (els cables poden passar per sota terra, etc..)
* Pot ser complicat ampliar la xarxa.
* Rendiment es deteriora al afegir-hi nodes.

Xarxes que implementen la topologia::
* Ethernet (versions obsoletes)
* Actualment no hi ha xarxes amb aquesta topologia

==== Topologia d’anell

.Topologia d'anell
image::images/topologiaanell.png[600, 600]

Quan una ordinador vol enviar una trama a un altre, aquesta ha de passar per a tots els que hi ha entremig.
El dispositiu que connecta l’ordinador a l’anell és un repetidor amb tres connexions.

Avantatges::
* Cada node actua com un repetidor i això permet que la xarxa tingui una gran extensió.

Inconvenients::
* Si falla algun enllaç la xarxa deixa de funcionar completament.
* Difícil de localitzar en quin punt hi ha la falla. (els cables poden passar per sota terra, etc..)

Xarxes que implementen la topologia::
* Token Ring (Obsoleta)
* FDDI (Fiber Distributed Data Interface) (En desús)
* SONET (Synchronous Optical Network)

==== Topologia d’arbre

.Topologia en arbre
image::images/topologiaarbre.png[600, 600]

* Es poc utilitzada ja que un error en un node deixa conjunts de nodes incomunicats.
* Es basa en un model jeràrquic dels nodes.
* No obstant s’utilitza en xarxes de telefonia on els enllaços del mig són centraletes locals i regional.

==== Topologia de malla

.Topologia en malla
image::images/topologiamalla.png[600, 600]

.Topologia en malla parcial
image::images/topologiamallaparcial.png[600, 600]

Aquesta topologia interconnexiona tots els nodes entre ells.

Avantatges::
* Si una ruta falla  es pot seleccionar una altra alternativa

Inconvenients::
* Elevat cost degut a la gran quantitat de cable.
* Dificultat de configuració degut a la varietat de camins que ajunten un node amb un altre.

Xarxes que implementen la topologia::
Topologia d'ús principalment en xarxes WAN amb enllaços punt a punt. Moltes vegades aquests enllaços són circuits virtuals.
+
Per exemple si tenim tres oficines connectades entre elles mitjançant VPNs tindríem una topologia d'aquest tipus.

==== Topologia en xarxes sense fils

Esta formada per àrees circulars o hexagonals cadascuna de les quals té un node individual en el centre.

En general la topologia de les xarxes sense fils és un àrea geogràfica dividida en regions (cel·les), per tant no existeixen enllaços físics.

Avantatges::
* Manca d’enllaços físics.

Inconvenients::
* Els senyals enviats per la xarxa es troben presents a qualsevol lloc de la cel·la i per tant són vulnerables a interferències i violacions de seguretat.

A nivell lògic existeixen tres topologies sense fils::
Ad-hoc:::
Una xarxa WANET (wireless ad hoc network) o MANET (Mobile ad hoc network) és un tipus de xarxa sense fils descentralitzada que no funciona sobre una infraestructura preexistent, commutadors o encaminadors o punts d'accés per exemple. Enlloc d'això cada node participa en l'enviament de les dades dels altres nodes.
+
És a dir, la selecció dels nodes que reenvien les dades es realitza dinàmicament en funció de la connectivitat i l'algoritme d'encaminament utilitzat.

Infraestructura Bàsica:::
Consisteix en una xarxa amb un únic *punt de redistribució*, per exemple un punt d'accés, junt amb un conjunt d'estacions "client" que estan associades a aquest punt de distribució.
+
Les estacions només es comuniquen amb el punt de distribució al que estana associades i tot el tràfic de la xarxa s'encamina a través d'aquest punt de distribució.

Infraestructura estesa:::
Esta formada per un conjunt d'infraestructures bàsiques en un mateix segment de xarxa.
+
D'aquesta manera les estacions es poden comunicar amb qualsevol dels punts de redistribució de la infraestructura.

.Topologies lògiques de les xarxes sense fils
image::images/topologiawireless.jpg[]

==== Topologia Irregular

* Cada node ha d’estar connectat, com a mínim per un enllaç.
* És la topologia més utilitzada per xarxes que ocupen zones geogràfiques amplies.
* Aquesta topologia permet la cerca de rutes alternativa quan falla algun dels enllaços. 

==== Topologia hibrida

Es parla de topologia hibrida en les xarxes formades per una agregació de d'ferents topologies més o menys identificables.

.Xarxa amb topologia hibrida
image::images/topologiahybrid.png[]

==== Topologia punt a punt

La topologia punt a punt (PTP) connecta dos nodes directament. Per exemple dos ordinadors connectats per mòdems, un terminal concertant-se a un mainframe o una estació de treball comunicant-se per un cable en paral·lel amb una impressora.
    
* En un enllaç PTP, dos dispositius monopolitzen el medi de comunicació. Donat que es comparteix el medi no fa falta un mecanisme per identificar als ordinadors i per tant no cal adreçament.
* Es en aquest cas quan té sentit parlar de símplex, dúplex i half-dúplex.

[WARNING]
====
No confondre la topologia punt a punt amb els protocols punt a punt. 
====

==== Topologies típiques de xarxes WAN

Interconnectar diferents xarxes a través de WANs involucra una varietat de tecnologies dels ISP i topologies WAN. Algunes d eles més comunes són:

Topologia punt a punt::
Normalment involucra una línia arrendada de l'estil T1 o E1
+
.Xarxa "Point to Point"
image::images/topologiaPuntaPunt.png[]
+
[NOTE]
====
Una línia E1 és una línia de comunicació digital desenvolupada per la transmissió de varies trucades telefoniques utilitzant multiplexació de divisió temporal.

T1 és una tecnologia similar implantada principalment a estats units mentre que E1 està implantada a Europa.

Actualment les línies E1 i T1 s'estan substituint per línies Ethernet.
====

Topologia Hub and Spoke::
S'utilitza quan fa falta connectar una connexió privada entre més d'un site.
+
Una sola interfície d'un dels encaminadors es comparteix per tots els circuits d'enviament dels demés encaminadors.
+
.Xarxa "Hub and Spoke"
image::images/topologiaHubandSpoke.png[]

Topologia Full Mesh::
Versió millorada de la topologia Hub and Spoke, utilitzant circuits virtuals cada site es pot comunicar directament amb els demés.
+
Per contra es complica la configuració i el manteniment.
+
.Xarxa "Full-mesh"
image::images/topologiaFullMesh.png[]

Topologia Dual Homed::
Proporciona redundancia i balanceig de càrrega a canvi d'un increment de hardware de xarxes i de complexitat en la configuració.
+
.Xarxa "Dual Homed"
image::images/topologiaDualHomed.png[]

==== Topologia lògica

La topologia lògica d’una xarxa  es la manera en que la informació es mou entre els hosts a través del mitjà de transmissió.

[IMPORTANT]
==== 
La topologia lògica d’una xarxa no coincideix necessàriament amb la seva topologia física. Per exemple, una xarxa Ethernet sol tenir una topologia física en estrella i un topologia lògica en bus. 
====

=== Classificació segons la transmissió de la informació

Aquesta classificació té en compte la manera en que es transmeten les dades d’un equip a un altre.

Amb aquest criteri la xarxa pot ser:

* Xarxa commutada
** Xarxa de commutació de circuits.
** Xarxa de commutació de paquets.
* Xarxa de difusió (Xarxa de Broadcast) 

==== ((Xarxes de  difusió)) (broadcast)

En una xarxa de difusió un equip envia la informació a  tots els nodes i és el destinatari l’encarregat de seleccionar i captar aquesta informació.

* Aquesta forma de transmissió està condicionada a la topologia de xarxa, aquesta ha de tenir un únic camí compartit per tots els nodes de la xarxa.
* Sembla  poc eficient però s’utilitza abastant en xarxes petites perquè no calen encaminadors ja que només existeix una ruta possible.

[NOTE]
====
Aquesta forma de transmissió està condicionada per la topologia de la xarxa ja que aquesta es caracteritza per tenir un únic “camí” de comunicació compartit per a tots els nodes. (per exemple per ones de radio).
====

[NOTE]
====
Cal notar que en aquest tipus de xarxes només existeix un únic camí possible.
====

==== ((Xarxes commutades))

A les xarxes commutades, un equip origen selecciona un equip receptor i la xarxa és l’encarregada d’habilitar una via de connexió entre ambdós.

La manera de que té la xarxa de sel·leccionar l'equip receptor determina el tipus de commutació de la xarxa.

[NOTE]
====
Sol haver-hi més d’un camí possible per a la connexió.
====

[NOTE]
====
No diem res sobre l’exclusivitat d’aquesta via. 
====

===== Xarxes de ((commutació de circuits))

Els equips de comunicació estableixen un camí físic entre els mitjans de comunicació previ a la connexió entre els equips.

Aquest camí resta actiu durant tota la comunicació i s’allibera al acabar.

Per exemple: 

La Xarxa de telefonia commutada (RTC)

En la commutació de circuits es segueixen els següents passos:

* Sol·licitud
* Establiment
* Transferència
* Alliberament de la connexió 

//^ 

Avantatges::
* Transmissió en temps real (útil per veu i vídeo)
* Acaparament de recursos. Els nodes que intervenen a la comunicació disposen en exclusiva del circuit mentre dura la transmissió.
* No hi ha *contenció*. Un cop establert el circuit, les parts es poden comunicar a la màxima velocitat permesa pel medi sense compartir l’ample de banda ni el tems d’utilització.
* El circuit es fix. No hi ha pèrdues de temps degudes a l’encaminament. Cada node té uns sola ruta pels paquets entrants i pels paquets sortints.
* Simplicitat en la gestió dels nodes intermedis.  Un cop establert el circuit no s’ha de prendre més decisions per encaminar les dades entre l’origen i el destí. 

Inconvenients::
* Retard en l’inici de la connexió. Es necessita un temps per a realitzar la connexió, no s’aprofita el circuit en els moments en que no hi ha transmissió.
* El circuit es fix. La ruta no es reajusta per readaptar-la a cada instant al camí de menor cost entre els nodes.
* Poc tolerant a errors. Si un node intermedi falla, tot el circuit falla i caldrà establir connexions des del principi.

===== Xarxes de ((commutació de paquets))

El missatge a enviar es divideix en fragments (anomenats paquets), cada fragment es llença a la xarxa i circula fins que arriba al seu destí.

* Els nodes intermedi emmagatzemen els paquets en cues. Aquestes memòries no necessiten ser massa grans.
* Cada paquet conté:
** Part de la informació a transmetre
** Informació de control
** Identificació del node origen
** Identificació del node destí 

Les xarxes de commutació de paquets poden operar en dues modalitats:

Modalitat de circuit virtual::
 * Cada paquet s’encamina pel mateix circuit que els anteriors, per tant s’assegura l’ordre d’arribada dels paquets a destí. 
Modalitat de datagrama::
* Cada paquet s’encamina de manera independent als demès i per tant la xarxa no pot controlar el camí que segueixen els paquets ni assegurar l’ordre de recepció al destí. 

//^ 

Avantatges::
* En cas d’error d’un paquet només es reenvia aquest paquet.
* Comunicació interactiva. Al limitar la mida dels paquets s’assegura que cap usuari no pugui monopolitzar la línia de transmissió durant molt de temps.(microsegons)
* Augmenta la  flexibilitat i la rendibilitat de la xarxa
* Es pot alterar sobre la marxa el camí seguit per una comunicació. (pe en cas d’averia d’un encaminador, o en cas de congestió)
* Es poden assignar prioritats als paquets de determinada comunicació. D’aquesta manera un node pot seleccionar de la seva cua de paquets aquells que seran retransmesos amb més urgència.

Inconvenients::
* Major complexitat en els equips de commutació intermedis, necessiten major capacitat i velocitat de càlcul per determinar la ruta adequada per a cada paquet.
* Duplicitat de paquets. Si un paquet tarda massa en arribar a destí el receptor pot considerar que s’ha perdut i enviar una sol·licitud de reenviament, podent donar lloc a paquets repetits.
* Si els càlculs d’encaminament representen un percentatge apreciable del temps de transmissió, el rendiment del canal disminueix.
