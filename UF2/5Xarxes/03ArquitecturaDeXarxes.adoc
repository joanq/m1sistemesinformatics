== Arquitectura de xarxes

=== Què vol dir definir una arquitectura de xarxa?

Al dissenyar una xarxa de comunicacions apareixen gran quantitat de problemes:

* Es produiran molts errors que s’hauran de corregir?
* Es compartirà un únic medi de transmissió?
* Com distingirem un ordinador d’un altre?
* Quin tipus d’informació s’ha de transmetre?
* Gestionarà informació confidencial?
* etc…

Cal una manera concreta i ben definida que resolgui cadascun dels problemes anteriors per exemple, caldrà definir coses com ara:

Encaminament::
* Si hi ha varies rutes entre l’origen i el destí caldrà establir uns mecanismes que garanteixin que un emissor sempre troba al receptor i ho fa pel camí més curt o més òptim.
Adreçament::
* Cal establir un mecanisme per que un procés d’una màquina pugui especificar amb qui vol comunicar-se. Cal posar noms als emissors i receptors per a poder dirigir els missatges al destí correcte.
Accés al medi::
* Si es comparteix el medi de transmissió (pe. broadcast) cal establir algun mecanisme que controli l’ordre de transmissió dels interlocutors per evitar que dos emissors emetin a l’hora.
Topologia::
* La topologia d’una xarxa té un impacte en tots els altres punts, una xarxa amb topologia punt a punt, per exemple, no necessita assignar nom als seus nodes ja que tot el que emeti un emissor anirà parar necessàriament a l’altre node de la xarxa.
Saturació del receptor::
* Cal gestionar el cas d’un emissor ràpid que satura un receptor lent.
Manteniment de l’ordre dels missatges enviats::
* Algunes xarxes desordenen els missatges que envien, cal establir un mecanisme que permeti tornar a ordenar-los.
Control d’errors::
* Cal establir mecanismes de detecció i de correcció d’errors. 
Multiplexació::
* La xarxa pot tenir trams en que, principalment per raons econòmiques, existeix un únic medi de transmissió compartit. Caldrà establir un mecanisme per a poder enviar dades de diferents emissors pel mateix canal a l’hora.

El conjunt de solucions concretes a tots els problemes anteriors, i alguns més, d’una xarxa concreta és el que es coneix amb el nom d'*arquitectura de la xarxa*.

Al llarg del temps han aparegut diverses arquitectures de xarxa i diverses estratègies per a construir-les. Actualment la majoria d’arquitectures de xarxa que s’utilitzen tenen unes característiques comunes, són les *((arquitectures per capes))*.

=== Arquitectures basades en capes

Abans de poder explicar què és una arquitectura per capes caldrà definir que és un *servei* i un *protocol de xarxa*.

Què és un ((servei))::
Un servei és el conjunt d’activitats que busquen respondre a les necessitats d’un client.
Què és un ((protocol de xarxa)) (o comunicacions)::
Conjunt de regles que especifiquen l’intercanvi de dades o ordres durant la comunicació de les entitats que formen part d’una xarxa.(en particular controlen, la construcció física de la xarxa, la connexió dels ordinadors a la xarxa, el format de les dades, el control dels errors…) 

Per exemple, alguns dels serveis del sistema telefònic són:

* Transmissió de veu
* Trucada en espera
* Transmissió de dades
* Trucada a tres
* El servei d’establiment de trucada és fonamental a la majoria de les xarxes, en alguns serveis no ho és. pe. enviament i recepció d’SMS
* La tarificació, es pot dur a terme per temps de connexió, quantitat de dades transmeses, etc…

i per exemple, el protocol que es segueix en una comunicació telefònica:

. Si es la tercera vegada que passa per aquí ves a 8
. Despenjar el telèfon
.. Comprovar si hi ha línia.
.. Si no hi ha línia tornar a 1
. Marcar el numero d’un altre usuari (sol·licitud)
. Esperar to
. Si el to és comunicant penjar i tornar a pas 1
. Si dona més de 6 tons i no contesta anar a 8
. Parlar quan l’altre usuari contesti (establiment i transferència)
. Penjar (alliberament) 

====  Capes, protocols i interfícies

Les primeres xarxes d’ordinadors es varen dissenyar tenint en compte el hardware com a punt principal i el software com a secundari.

Aquesta estratègia ja no funciona i actualment el software de xarxes està altament estructurat.

La filosofia actual es resumeix en DIVIDEIX I GUANYARAS 

*Per reduir la complexitat a nivell de disseny, la majoria de xarxes s’organitzen com una pila de capes o nivells*.


==== Característiques de les capes o nivells (layers)

* Dintre de cada nivell coexisteixen varis serveis.
* Els serveis estan definits mitjançant protocols estàndards
* Cada nivell es comunica únicament amb el nivell immediatament superior y amb immediatament inferior.
* Cada nivell inferior proporciona serveis al seu nivell immediatament superior.
* El nombre de capes , així com el nom i el contingut difereixen en cada xarxa.
* L’objectiu d’una capa és proporcionar certs serveis a les capes superiors, a les
quals no se’ls hi mostren els detalls reals de la implementació dels serveis que ofereixen.
* La capa n d’una màquina manté una conversa amb la capa n d’una altra màquina.

==== Funcions dels protocols

Algunes funcions dels protocols són:

* Detectar la connexió física existent
* Establir una connexió (handshake)
* Negociació de les característiques de la comunicació (velocitat, tassa d’error mínima, etc.)
* Quan s’inicia o acaba un missatge i quin format té.
* Que fer amb els missatges incorrectes o mal formats
* Com acabar una connexió
* Com detectar un final inesperat d’una connexió…

.Algunes associacions que determinen protocols
****
* IEEE (Institute of Electrical and Electronic Engineering)
* ANSI (American National Standards Institute)
* TIA (Telecommunications Industry Association)
* EIA (Electronic Industries Alliance)
* ITU (International Communications Union) 
****

==== Protocol de capa n

Les regles i convencions utilitzades en la conversació es coneixen de manera col·lectiva com a protocol de capa n.

Les entitats que abracen les capes corresponents en diferents màquines s’anomenen iguals (o peers). Els iguals podrien ser processos, dispositius de hardware o fins i tot persones.

En realitat les dades no es transfereixen de manera directa des de la capa n d’una màquina a la capa n de l’altre, sinó que cadascuna passa les
dades i la informació de control a la immediatament inferior. 

A més a més, a nivell intern entre cada parell de capes adjacents hi ha una *interfície*.

La *interfície* defineix quines operacions i quins serveis primitius posa la capa més baixa a disposició de la capa superior immediata.

.Capes, protocols i interfícies
image::images/capes.png[]

.Exemple del funcionament de les capes
image::images/capesexemple.png[]

=== Arquitectura de xarxa

L’arquitectura d’una xarxa és el conjunt organitzat de capes i protocols de la mateixa.

* Aquesta organització ha d’estar prou clara per què els fabricants de software i de hardware construeixin productes compatibles.

[NOTE]
====
l’arquitectura no contempla a les interfícies ja que l’arquitectura de capes les oculta totalment. 
====

=== Models de referència

De totes les arquitectures que treballen per capes n’hi ha dues que mereixen una especial atenció.

Com a model teòric prendrem l’anomenat *model de referència OSI* i com a implementació pràctica estudiarem el *model TCP/IP*, arquitectura en la que es basen la majoria de xarxes d’ordinadors actuals.

=== Model de referència ((OSI)) (Open systems Interconnection)

Model basat en una proposta desenvolupada per la ISO com a primer pas per a l’estandardització internacional dels protocols utilitzats en varies capes.

* Primera versió 1983, revisat al 1995
* La primera versió no definia ni els serveis ni els protocols de cada capa, només definia la funció general de cada una d’elles.
* Els protocols associats al model OSI gairebé ja no s’utilitzen (definits en
 posteriors versions), el model no obstant és molt general i encara és vàlid, i les característiques tractades en cada capa encara són molt importants.
* El model OSI té 7 capes. 

Els principis emprats per la definició de les capes varen ser els següents:

. Cada capa ha de realitzar una funció ben definida.
. La funció de cada capa s’ha de triar amb la intenció de definir protocols estandarditzats internacionalment.
. Els límits d eles capes s’han de triar amb la idea de minimitzar el flux d’informació a través de les interfícies.
. La quantitat de capes ha de ser lo suficientment gran per no haver d’agrupar funcions diferents a la mateixa capa i lo suficientment petita per què l’arquitectura no es torni immanejable.
. S’ha de crear una capa sempre que  faci falta realitzar una funció ben diferenciada de la resta.
. Permetre que les modificacions de funcions o protocols que es realitzin en una capa no afectin als nivells contigus.
. Utilitzar l’experiència de protocols anteriors. Les fronteres entre capes s’han
de situar on l’experiència ha demostrat que són convenients.
. Cada capa ha d’interactuar únicament amb les capes immediatament superior i immediatament inferior. 

.Model OSI
image::images/modelOSI.png[]

==== Capa 1 - Física

Té que veure amb la transmissió de dígits binaris per un canal de comunicació. Les consideracions de disseny consisteixen en assegurar que quan un emissor envia un 1 el receptor rep un 1.

Les preguntes típiques són:

* Quin voltatge s’ha d’utilitzar per representar un 1, i un 0?
* En quina freqüència de radio es transmetrà?
* Quantes puntes té el connector de xarxa i per què serveix cadascuna? 

Principals funcions:

* Definir el medi físic pel que viatjarà la comunicació (cable de parells trenats, ones de radio, etc…)
* Definir els components materials i elèctrics (connectors i nivells de tensió)
* Establiment, manteniment i alliberament de l’enllaç físic.
* Transmetre el flux de bits a través del medi.
* Unitat de treball: *bit*
* Només té en compte els veïns immediats 

==== Capa 2 – Enllaç

La capa d'enllaç proporciona *transferència de dades en enllaços amb dos  nodes connectats directament*.

La capa d’enllaç està formada per dues subcapes anomenades:

. MAC, Medium Access Control
. LLC, Logical Link Control

Responsabilitats de la Subcapa MAC::
Encapsulació de dades:::
* Muntatge de la trama abans de la retransmissió i desmuntatge de la trama en el moment de la recepció.
* La capa MAC afegeix una capçalera i un final al PDU de la capa de xarxa.
Delimitació de trames:::
* Identifica un grup de bits que componen una trama; sincronitza els nodes emissor i receptor.
* La longitud de les trames depen del mitjà de transmissió i s'anomena ((*MTU*)), Medium Tranfer Unit.
* Si el paquet enviat per la capa de xarxa és més gran que el MTU de l'enllaç, aquesta capa pot *fragmentar* el paquet i posar-lo en més d'una trama. Això és poc habitual, en general s'intenta que la mida dels paquets sigui inferior al MTU.
Adreçament:::
* Gestiona la identificació d’emissors i receptors en els entorns de broadcast.
Detecció d'errors:::
* Cada trama Ethernet conté un final de trama amb una comprovació de redundància cíclica (CRC) del contingut de la trama.
Control d'accés al mitja de transmissió:::
* Responsable de la ubicació i la retirada de trames en els mitjans de transmissió.
* Es comunica directament amb la capa física.
* Si més d'un dispositiu comparteixen el mateix mitjà de transmissió i intenten enviar dades simultàniament aquestes col·lisionen i es corrompen, Ethernet proporciona un mètode per controlar la manera en que els nodes comparteixen l'accés mitjançant l'ús d'una tecnologia anomenada *CSMA (Carrier Sense Multiple Access)*.

Responsabilitats de la Subcapa LLC::
** Multiplexar els diferents protocols que transmeten sobre la capa MAC quan es transmet i descodificar-los quan es rep.
** Proporcionar el flux de comunicació i el control d'errors d'aquesta.

//^ 

* Unitat de treball: ((*trama*)) o *frame*
* Només té en compte els veïns immediats.

==== Capa 3 – Xarxa

S'ocupa de la transferència de seqüències de dades de longitud variable d'un node a una altre node *d'una xarxa diferent*.

S’ocupa de determinar la millor ruta per la que enviar la informació. Aquesta decisió té que veure amb:

* el camí més curt
* el més ràpid
* el de menor tràfic

Principals funcions::
** Gestió de congestions (evitar embussos). (Repartir la càrrega equilibradament entre les diferents rutes).
** Assegura la qualitat de servei
*** retards
*** temps de transit
*** inestabilitat, etc…
* Amaga a les capes superiors els diferents formats missatges de la capa d’enllaç, converteix i adapta els missatges entre xarxes heterogènies acapa 2.
*** Diferent mida de trama
*** Diferents protocols de capa 2
*** Diferent adreçament físic
** Proporciona un adreçament lògic que permet identificar emissors i receptors en un àmbit de inter-xarxes.
* Només té en compte els veïns immediats
* Unitat de treball: *Paquet*

==== Capa 4 – Transport

Principals funcions::
** Transició entre els nivells orientats a xarxa i els nivells orientats a aplicació, dit d’una altra manera, té la responsabilitat d’agafar les dades provinents del nivell de sessió i passar-los al nivell de xarxa.
** Representa la connexió d’extrem a extrem (en aquest punt no importa el que passi per sota i ja no es té en compte només els veïns)
** Té la missió d’aïllar les capes superiors dels canvis inevitables de la tecnologia del maquinari.
** Proporciona, en funció dels protocols concrets, les següents funcionalitats:
*** *Segmentació* i reensamblatge de les dades en PDUs anomenats *segments* que seran la càrrega dins dels paquets de capes inferiors.
*** Proporciona mecanismes de *recuperació d'errors*
*** Gestiona les confirmacions de recepció tant positives, *ACK*, com negatives *NACK*.
*** Gestiona els temps d'espera per a les retransmissions.
*** Gestiona la *retransmissió* de les dades.
*** Control del flux d’enviament de dades, negocia la velocitat d'enviament de les dades.
*** Determina el tipus de servei que proporcionarà a la capa de sessió i finalment als usuaris de la xarxa. Aquest servei pot ser:
**** *Servei orientat a connexió sense confirmació de recepció*: + 
Es basa en el sistema telefònic, l’usuari en primer lloc estableix una connexió, la utilitza i finalment l’abandona. La connexió funciona com un tub, l’emissor empeny els bits en un extrem i el receptor els rep en l’ordre en que s’han enviat. 
**** *Servei no orientat a la connexió amb confirmació de recepció*: +
Es concep en base al sistema postal, cada missatge porta la direcció de destí i s’encamina través del sistema independentment dels demés,l’ordre de recepció no està garantit). Es coneix amb el nom de servei de datagrames. 
**** *Servei orientat a la connexió amb confirmació de recepció:* +
Diem que és un servei *fiable* en el sentit que mai perd dades. El procés de confirmació de recepció introdueix retards i sobrecarregues a la comunicació.

** Controla que un cop arribades les dades al node destí aquestes passin a *l’aplicació correcte*.

//^ 

* És la capa més baixa que té independència total del tipus de xarxa utilitzada.
* Unitat de treball: *TPDU* (Transport protocol data unit) o *segment*

==== Capa 5 – Sessió

S’estableixen sessions de comunicació entre els dos extrems pel transport ordinari de dades. Gestiona les connexions entre usuaris finals (processos).

És una capa dissenyada pensant en la xarxa telefònica commutada, fora d'aquesta xarxa és una capa que no tindrà massa sentit.

Principals funcions::
** Es proporcionen alguns serveis millorats, pe. la restabliment de la conversa després d’un error.
** Es responsable de diferenciar entre les diferents connexions garantint que les dades s’envien per la connexió correcta (en el fons l’aplicació correcta).
** Control de la concurrència

//^ 

* Unitat de treball: *SPDU*

==== Capa 6 – Presentació

Aquest nivell controla el significat de la informació que es transmet.

Principals funcions::
** És responsable de definir com es presenta la informació a l’usuari a l’interfície que estigui usant.
*** Semàntica i sintaxis de la informació transmesa (pe. ASCII i EBCDIC)
*** En el cas d’una comunicació segura codifica i encripta les dades.

//^ 

* Unitat de treball: *PPDU*

==== Capa 7 – Aplicació

Està en contacte directe amb els programes o aplicacions de les estacions.

Principals funcions::
** Conté els protocols que demanen els usuaris
** Normalment l’usuari no interactua directament amb aquesta capa, sinó que utilitza aplicacions que hi interactuen. (pe. navegador web)
** Proporciona la gestió d’errors i la integritat e les dades entre dues aplicacions en diferents hosts.

//^ 

* Unitat de treball: *APDU*

==== Unitats de dades dels protocols OSI

Les unitats de dades dels protocols, també anomenats PDU, s’utilitzen per l’intercanvi de dades entre capes d’una pila de protocols.

Cada PDU contè informació de control i possiblement dades d’usuari.

.PDU al model OSI
image::images/pdus.png[1000,1000]

==== Problemes amb el model OSI

El model OSI no ha estat mai adoptat a la pràctica per la implementació de xarxes, s’utilitza més aviat com un model teòric, alguns els problemes que ho van produir són:

* Algunes capes estan pràcticament buides (pe. sessió i aplicació) mentre que algunes estan massa plenes (pe. física, enllaç de dades i Xarxa).
* Algunes funcions es repeteixen en moltes capes, lo que produeix increment en les capçaleres de control dels blocs d’informació i duplicitat de serveis. (pe. control d’errors  i adreçament)
* Les implementacions dels protocols eren dolents, lo que va fer agafar mala fama al model.

===  Model de referència TCP/IP

El model TCP/IP de 4 capes, és el model de referència utilitzat per ARPANET i posteriorment Internet, actualment es treballa amb un model TCP/IP de 5 capes que permet distingir la capa física de la capa d'enllaç.

L’arquitectura  TCP/IP es va construir dissenyant inicialment els protocols i desprès es van integrar en capes, just al revés que OSI. 

Els principis de disseny d’aquest model varen ser:

* Ha de permetre interconnectar xarxes diferents, és a dir, la xarxa general pot estar formada per trams que utilitzen tecnologies de transmissió diferents.
* Ha de ser tolerant a fallades. El DoD volia una xarxa que pogués suportar atacs terroristes sense perdre dades i mantenint les comunicacions.
* Ha de permetre l’ús d’aplicacions diferents, transferència d’arxius, comunicació en temps real, etc…


.Comparació dels models OSI i TCP/IP
[cols="1,4,4,4", options="autowidth"]
|====
| ^h| OSI       ^h| TCP/IP (4 capes) ^h| TCP/IP (5 capes)
^| 7 | Aplicació .3+.^| Aplicació .3+.^| Aplicació
^| 6 | Presentació 
^| 5 | Sessió 
^| 4 | Transport | Transport | Transport
^| 3 | Xarxa | Internet | Xarxa
^| 2 | Enllaç .2+.^| Accés a la xarxa | Enllaç
^| 1 | Física | Física
|====

==== Capa 1 - Link (accés a la xarxa)

* El model dona poca informació sobre aquesta capa.
* Només especifica que ha d’existir algun protocols que connecti l’estació a la xarxa. +
La raó és que com que TCP/IP es va dissenyar per que funcionés sobre xarxes diferents, aquesta capa depèn de la tecnologia utilitzada i no s’especifica prèviament. 

* Protocols importants: 
** Ethernet (IEEE 802.3)
** Wi-fi (IEEE 802.11)
** ZigBee (IEEE 802.15.4)
** PPP
** PPPoE
** PPPoA 
** DSL
** ISDN
** Frame Relay
** IEEE 802.16 WiMAX


==== Capa 2 – Internet

Té l’objectiu que les estacions enviïn paquets a la xarxa i que aquests viatgin de forma independent al seu destí.

* Els paquets poden travessar xarxes diferents i poden arribar desordenats.

* Protocols Importants:
** ((IP)) (Internet Protocol)
*** Aquest protocol defineix un mecanisme d’entrega de paquets no fiable i no orientat a connexió.
*** És a dir, no garanteix l’arribada dels paquets, ni l’ordre d’arribada.
*** Actualment s’utilitzen dues versions del protocol anomenades IPv4 i Ipv6 
** ((ICMP)) (Internet Control Message Protocol)
*** El protocol ICMP es va dissenyar inicialment per permetre que els routers poguessin informar de quina era la causa dels errors d’entrega de paquets IP. 
*** L'ús del protocol no està restringit exclusivament als routers una màquina arbitraria pot enviar un missatges ICMP a qualsevol altra màquina. 

==== Capa 3 - Transport

La seva missió és establir una conversa entre origen i destí, igual que la capa de transport del model OSI.

* Té la responsabilitat de controlar l’arribada de tots els paquets.
* Avisar si cal de l’arribada d’un paquet o la no arribada d’aquest.
* Ordenar els paquets rebuts en cas de recepció desordenada.
* Protocols principals:
** ((TCP)) (orientat a la connexió i fiable)
*** Servei orientat a la connexió amb confirmació de recepció. (Diem que és un servei fiable en el sentit que mai perd dades. El procés de confirmació de recepció introdueix retards i sobrecarregues a la comunicació). 
** ((UDP)) (no orientat a la connexió i no fiable) 
*** Servei no orientat a la connexió amb confirmació de recepció (Es concep en base al sistema postal, cada missatge porta la direcció de destí i s’encamina través del sistema independentment dels demés, l’ordre de recepció no està garantit). Es coneix amb el nom de servei de datagrames. 

==== Capa 4 - Aplicació

* Conté tots els protocols d’alt nivell que utilitzen les aplicacions per a comunicar-se, TELNET, FTP, HTTP, SMTP, DNS 

===== Protocols del model TCP/IP

.Alguns protocols del model TCP/IP
[cols="7*^", options="autowidth"]
|====
.2+.^| Aplicació | DNS | FTP | DHCP | HTTP | HTTPS | SSH
| telnet | SNMP | POP | SMTP | RIP | BGP
| Transport 3+| TCP 3+| UDP
| Internet | IPv4 | IPv6 | ICMP | ICMPv6 | IPSec | ARP
.2+^| Enllaç de dades 3+| PPP 3+| Ethernet (IEEE 802.3)
3+| Wi-fi (IEEE 802.11) 3+| ZigBee (IEEE 802.15.4)
|====

==== Problemes amb el model TCP/IP

* No es pot utilitzar com a model
** El model original de TCP/IP no distingia els conceptes de capa, servei, interfície i protocol, i per tant no es pot utilitzar com a guia per dissenyar xarxes en noves tecnologies
* Alguns dels protocols son molt específics i les implementacions es van distribuir de forma lliure per tant han quedat obsolets (per ser específics) però s’ha difós massivament lo que els fa difícilment substituïbles.
* Per exemple,  TELNET, es va dissenyar per una terminal mecànica de 10 caràcters per segon, no sap res d’interfícies gràfiques però 25 anys després encara s’utilitza.

===  Comparació entre el model OSI i el model TCP/IP

* OSI defineix clarament entre servei, interfície i protocol
** Servei: Les responsabilitats de la capa
** Interfície: Accessibilitat de la capa
** Protocol: Implementació dels serveis.
* TCP/IP no té clara aquesta separació
* OSI es va definir abans d’implementar els protocols, els dissenyadors no tenien experiència amb on s’havien d’ubicar les funcionalitats, algunes funcionalitats falten. (pe. OSI no suportava broadcast originalment)
* El model TCP/IP es va definir desprès dels protocols i s’adeqüen perfectament.

OSI no va tenir èxit degut a:

* Mal moment d’introducció:
** Poc temps per les investigacions i el desenvolupament del mercat per a aconseguir l’estandardització.
* Mala tecnologia:
** OSI és complex, es dominat per una mentalitat de telecomunicacions sense pensar en ordinadors (pe. no suporta serveis sense connexió).
* Males polítiques:
** Investigadors i programadors en contra els ministeris de telecomunicacions.

En definitiva:

*OSI és un bon model i TCP/IP és un bon conjunt de protocols*.
