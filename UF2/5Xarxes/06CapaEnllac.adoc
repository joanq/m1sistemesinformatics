== Capa d'enllaç

.Torre OSI
image::images/portada.png[]

////
=== Ethernet

[TIP]
====
Característiques d'Ethernet:

* Tecnologia LAN més utilitzada
* Opera a la capa d'enllaç de dades i a la capa física.
* Família de tecnologies de xarxes definida als estàndards IEEE 802.2 i 802.3.
* Admet amples de banda de dades de 10, 100, 1000, 10 000, 40 000 i 100 000 Mbps (100 Gbps).
====

Estàndards d'Ethernet:

* Defineixen els protocols i les tecnologies de capa 2.
* Operen en dues subcapes separades de la capa d'enllaç de dades la subcapa LLC (Logical Link Control) i la subcapa MAC (Medium Acces Control).

=== Capa d'enllaç

La capa d'enllaç es divideix en dues subcapes:

* Subcapa MAC - Medium Acces Control
* Subcapa LLC - Logical Link Control

=== Subcapa MAC

* Recordem que les xarxes es poden dividir en dos categories, les que utilitzen connexions punt a punt i les que utilitzen canals de difusió.
* La subcapa MAC té sentit en les xarxes de difusió i resol el següent problema:

[IMPORTANT]
====
Com determinar qui pot utilitzar el canal quan hi ha competència per ell?
====

=== Subcapa LLC

* La finalitat d'aquesta subcapa és:

[IMPORTANT]
====
Aconseguir una comunicació confiable i eficient entre dues màquines adjacents en la capa d’enllaç de dades (ie. Estan connectades per un canal, conceptualment un cable.
====

==== Responsabilitats de la Subcapa MAC

Encapsulació de dades::
* Muntatge de la trama abans de la retransmissió i desmuntatge de la trama en el moment de la recepció.
* La capa MAC afegeix una capçalera i un final al PDU de la capa de xarxa.
Delimitació de trames::
* Identifica un grup de bits que componen una trama; sincronitza els nodes emissor i receptor.
Adreçament::
* Cada encapçalament Ethernet que s'afegeix a la trama conté l'adreça física (adreça MAC) que permet que la trama s'entreguin al node de destí.
Detecció d'errors::
* Cada trama Ethernet conté un final de trama amb una comprovació de redundància cíclica (CRC) del contingut de la trama.
Control d'accés al mitja de transmissió::
* Responsable de la ubicació i la retirada de trames en els mitjans de transmissió.
* Es comunica directament amb la capa física.
* Si més d'un dispositiu comparteixen el mateix mitjà de transmissió i intenten enviar dades simultàniament aquestes col·lisionen i es corrompen, Ethernet proporciona un mètode per controlar la manera en que els nodes comparteixen l'accés mitjançant l'ús d'una tecnologia anomenada *CSMA (Carrier Sense Multiple Access)*.

.Responsabilitats de la subcapa MAC
image::images/capa2tasks.png[]

==== Responsabilitats de la Subcapa LLC

* Multiplexar els diferents protocols que transmeten sobre la capa MAC quan es transmet i descodificar-los quan es rep.
* Proporcionar el flux de comunicació i el control d'errors d'aquesta.

[WARNING]
====
A les xarxes actuals el control de flux i la gestió d'errors es proporcionada per la *capa de transport* enlloc i no per la capa d'enllaç.
====

==== Problema: Identificació de trames

Descripció del problema::
Determinar on comença i on acaba una trama.

* Algunes de les tècniques per dividir en trames el flux de bits són:
** Recompte de bits.
** Banderes amb farciment de bits.
** Infraccions en l’estàndard de codificació de la capa física.

===== Tècnica: Recompte de bits

[IMPORTANT]
====
Aquesta tècnica consisteix en afegir un camp a la capçalera de la trama, especificant-ne el nombre de *bits*.
====

* En el moment en que la capa d'enllaç veu, a l'extrem del destinatari, el recompte de bits, coneix el nombre de bits de la trama i per tant coneix a on acaba.
* El problema amb aquesta tècnica és que el recompte es pot distorsionar per un error a la transmissió.
* Encara que el receptor fos capaç de detectar que la trama és incorrecta no tindria manera de saber a on comença la següent trama.

.Recompte de bits
====
Suposem que volem enviar les següents dades mitjançant la tècnica de recompte de bits suposant que el camp longitud ocupa 8 bits.

Dades:

----
01111010 00100110 00000000 11111111 
----

En total hi ha 32 bits més els 8 bits del camp de longitud, per tant la trama a enviar serà:

Trama: 

----
00101000 01111010 00100110 00000000 11111111
----

NOTE: 40~10~ = 00101000~2~ 
====

===== Tècnica: Farciment de BITS

[IMPORTANT]
====
Aquesta tècnica s'utilitza en protocols _orientats a bit_ (tracten la informació com bits individuals, no com a caràcters).

És semblant a la tècnica de Caràcters d'inici i fi amb farciment de caràcters però es pren ESC com  a un bit enlloc de com a un caràcter.
====

* Es defineix una única seqüència de control anomenada *FLAG* que s'utilitza per delimitar el principi i el final de cada trama.
* Prenem la seqüència: *01111110*
* Per tant, per mantenir la integritat de les dades cal evitar que dins d'una trama aparegui la seqüència FLAG.
** Per fer-ho, n'hi ha prou en que l'emissor inserti un *0* cada cop que detecta una seqüència de 5 bits *1* seguits. (Que no sigui en un FLAG)
** El receptor cada cop que rep una seqüència de 5 bits *1* seguits, comprova el bit següent:
*** Si és un *0* l'elimina. (Sap que és un bit auxiliar)
*** Si és un *1* Sap que es tracta d'un principi/final de trama
* Els protocols HDLC, SDLC i *PPP* (en mode síncron) utilitzen aquesta tècnica

.Farciment de bits
====
Cadena de bits a transmetre:

----
01111101  01111110  01101111 11110100
----

Trama enviada al medi físic: 

[subs="quotes"]
----
01111110 011111**0**01  011111**0**10  01101111 1**0**1110100 01111110
----

Cadena de bits al receptor:

----
01111101  01111110  01101111 11110100
----

====

===== Tècnica: Infraccions en l’estàndard de codificació de la capa física

[IMPORTANT]
====
Aquesta tècnica es basa en utilitzar valors no permesos en l'estàndard d'enviament de dades per identificar el principi i el final de les trames.
====

* Per exemple una xarxa que utilitza dos bits físics per cada bit lògic deixa dues combinacions per a senyalització.

==== Problema: Adreçament a la capa d’enllaç

[IMPORTANT]
====
L'adreçament permet identificar l'origen i el destí en un enllaç de dades que connecta a un conjunt de potencials emissors o receptors d'informació.

Les adreces assignades a la capa d'enllaç s'anomenen *adreces MAC* o *adreces físiques*
====

===== Adreçament implicit

En connexions *punt a punt* entre dos nodes, les trames que envia un node van adreçades  forçosament al node de l'altre extrem.

En aquest cas no es necessari que les adreces estiguin indicades explícitament a la trama.

===== Adreçament per presel·lecció

En alguns busos, per exemple IEEE-488, un dels nodes actua com a controlador i presel·lecciona, prèviament a la transmissió de trames, un node emissor i un node receptor.

Aleshores l'origen pot enviar un nombre definit de missatges cap el destí.

Les trames no contenen explícitament les adreces d'origen i de destí ja que no hi pot haver cap altre node actiu en el moment de la transmissió.

===== Adreçament en sistemes amb un únic "Master"

En molts sistemes de comunicació de caràcter industrial, entre els elements connectats al mateix medi de transmissió, existeix un node únic que funciona com a *_master_* i tots els altres nodes que funcionen com a *_slaves_*.

Tots els intercavis de trames es realitzen entre el *_master_* i un *_slave_* de manera que entre dos nodes esclau mai hi ha una comunicació directa, sempre es passa pel node *_master_*. I per tant:

* Les trames contenen una sola adreça,
** La d'origen quan la trama va d'un esclau al "master"
** la de destí quan la trama viatge del "master" a un esclau.

===== Adreçament amb més d'un "Master"

En aquest cas qualsevol node connectat al mitjà de transmissió pot enviar una trama a qualsevol altre.

La trama ha de contenir les dues adreces, la d'origen i la de destí.
////

=== Adreçament físic a les xarxes Ethernet

L’adreçament consisteix en la capacitat d’una xarxa d’identificar una interfície de xarxa d’entre totes les demés.

L’objectiu és poder enviar trames d’un host a un altre  sense que hi hagi dubte ni de l’emissor ni del receptor de la informació.

En general l’adreçament es tracta a tres nivells del model OSI:

* La capa d’*enllaç*
* La capa de *xarxa*
* La capa de *transport*.

L’adreçament a la capa d’enllaç té sentit quan el medi de transmissió és un medi compartit, *en el cas de les WAN, que utilitzen enllaços punt a punt per enllaçar els seus nodes es pot obviar l’adreçament en aquest nivell*.

S'assignen adreces MAC a totes  les NIC de tots els nodes de xarxa, això és, a les estacions e treball, als servidors, a les impressores, als switchos i als routers, etc...

Característiques::

* L'adreça MAC és individual i cada adaptador de xarxa té la seva pròpia adreça MAC *gravada de fàbrica* de manera que no pot ser modificada.
*   Aquestes adreces poden tenir major o menor nombre de dígits en funció de l’estàndard utilitzat.
* En el cas *d'Ethernet*, *Bluethooth*, Token Ring i *Wifi* tenen *48 bits*
** Per exemple: 
*** MAC: 00-05-9A-3C-78-00, 00:05:9A:3C:78:00 y 0005.9A3C.7800.
* En el cas d’Ethernet, els *24 primers bits els assigna la IEEE* i els *24 bits restants els assigna el fabricant*.
** En d’altes tipus de xarxes les adreces MAC poden ser determinades per l’usuari (pe, ARCNet)
* Els commutadors (switch) poden distingir de manera automàtica una estació d’una altra obtenint l’adreça MAC de les estacions de treball, associant-la al port on està connectada i mantenint les associacions en una taula a la RAM.
* Una adreça MAC de Ethernet de capa 2 es un valor binari de *48 bits* expressat como a *12 dígits hexadecimals*.
* Les notacions més habituals són:
** *xx:xx:xx:xx:xx:xx* on xx són dos dígits en hexadecimal
** *xx-xx-xx-xx-xx-xx* on xx són dos dígits en hexadecimal
** *xxxx.xxxx.xxxx* on xx són dos dígits en hexadecimal

El IEEE obliga a los proveïdors a respectar dues normes::
* Han d'utilitzar el OUI (Organization Unique Identifier) assignat al proveïdor en els 3 primers bytes.
* S'ha d'assignar un valor únic i exclusiu a cadascuna de les adreces MAC amb el mateix OUI (els 3 últims bytes).

Existeixen pàgines Web especifiques per a buscar la correspondència entre els diferents fabricants, http://w3dt.net/tools/maclookup/

[NOTE]
====
La IEEE controla més d’un estàndard de direcció física, MAC-48, EUI-48 i EUI-64
====

[TIP]
====
En xarxes locals hi ha la possibilitat d’assignar una MAC manualment i sobreescriure lògicament la MAC original.
====

.MAC Address
image::images/macaddress.png[]

La relació de les adreces MAC i les trames en una xarxa Ethernet és la següent:

* Es reenvia el missatge a una xarxa Ethernet, s'afegeix informació a la capçalera  indicant la MAC origen i la MAC destí.
* Cada NIC revisa la informació per verificar si l'adreça MAC de destí de la trama coincideix amb l'adreça MAC física del dispositiu, emmagatzemada a la RAM.
** *Si no hi ha coincidència la trama es descarta*.
** Si hi ha coincidència la NIC passa la trama a les capes superiors.

[IMPORTANT]
====
En el cas d'Ethernet existeixen tres tipus de MAC diferents en funció de com es comporten com a adreça de destí. Les MAC poden ser:

* *Unicast*
* *Broadcast*
* *Multicast*
====

.Exemple MAC unicast
image::images/macunicast.png[]

.Exemple MAC broadcast
image::images/macbroadcast.png[]

.Exemple MAC multicast
image::images/macmulticast.png[]

Per distingir l'àmbit d'una adreça MAC cal fixar-se en alguns dels seus bits:

* Les adreces *unicast* tenen un *0* al *vuitè* bit.
* Les adreces *multicast* tenen un *1* al *vuitè* bit.
* Només existeix una adreça MAC de *broadcast*, la seva representació binària és 48 bits a *1*, en hexadecimal es representa per *FF:FF:FF:FF:FF:FF*

[IMPORTANT]
====
*No* totes les MAC equivalen a un dispositiu físic real.

Les *adreces MAC* tenen els 24 primers bits registrats a la IEEE, és a dir, que fan referència a un dispositiu físic real s'anomenen *adreces MAC OUI* "Organizationally Unique Identifier"
====

* A vegades interessa indicar que una MAC no pertany a un dispositiu real per a donar-li un tractament de xarxa específic. En aquest cas es pot indicar que la MAC no és *OUI*

Per distingir si una adreça MAC és o no OUI cal fixar-se en alguns dels seus bits:

* Les adreces MAC OUI tenen el *setè* bit a *0*.
* Les adreces MAC *no* OUI tenen el *setè* bit a *1*.

.Adreces MAC de multicast i OUI
image::images/macbits.png[1000,1000]

////
=== El protocol ARP

[IMPORTANT]
====
Els nodes d'una xarxa Ethernet desconeixen les adreces MAC dels altres nodes de la xarxa, per tant no poden formar trames indicant l'adreça de destí.

En general, però, si que coneixen l'adreça IP de destí.

El *protocol ARP* proporciona un mecanisme per a trobar les adreces MAC dels nodes de destí de la mateixa xarxa a partir de la seva IP.
====

[WARNING]
====
El protocol ARP és un protocol *local*, és funcional en el segment de "broadcast" on es troben el node emissor i el node receptor, els paquets ARP no atravessen *dominis de difusió*.

El protocol ARP és especific de IPv4, en IPv6 és substituït pel protocol NDP "Network Discovery Protocol"
====

[IMPORTANT]
====
* El protocol ARP *resol adreces IPv4 a adreces MAC*
* Manté la taula d'assignacions ARP.
====

image::images/arp0.png[]

Taula ARP::
* S'utilitza per trobar l'adreça de capa 2 assignada a l'adreça IPv4 de destí.
* A mesura que un node rep trames, es registren les adreces IP i MAC d'origen com a entrades a la taula ARP.
* Es possible introduir entrades manualment a la taula ARP.

Sol·licituds ARP:
* Broadcast de capa 2 a tots els dispositius LAN Ethernet.
* El node que coincideix amb l'adreça IP del broadcast és l'únic que respon.
* Si cap dispositiu respon a la sol·licitud ARP, el paquet es descarta ja que no es podria crear una trama.

image::images/arp1.png[]

image::images/arp2.png[]

image::images/arp3.png[]

image::images/arp4.png[]

image::images/arp5.png[]

image::images/arp6.png[]

[WARNING]
====
*Què passa si el destí no esta a la mateixa LAN que l'origen?*
====

*Si el host IPv4 de destí NO es troba a la mateixa LAN que l'origen, la petició ARP no es realitza contra el node receptor sinó contra el router de sortida (*default gateway*) de la xarxa LAN.

[IMPORTANT]
====
La informació recopilada pel protocol ARP s'emmagatzema en una *memòria cau* als nodes que l'han sol·licitada.
====

Per evitar inconsistències entre les correspondències entre adreces MAC i adreces IP dels nodes de la xarxa *un temporitzador de caché ARP elimina les entrades ARP que no s'han utilitzat durant un període de temps especificat*.

* També es poden utilitzar comandes per eliminar manualment entrades de la taula ARP.
* I també hi ha la possibilitat de fer algunes de les entrades ARP *permanents*

[source, bash]
----
La comanda que gestiona les entrades a la taula ARP és la comanda arp (Windows/Linux):

arp -a # Mostra la taula arp
arp -s # Permet fer una entrada permanent
arp -d # permet eliminar manualment entrades de la taula ARP
----

.Eliminació de la memòria cau ARP
image::images/arp7.png[]

.Mostrar taula arp en IOS
image::images/showiparp.png[1000,1000]

.Mostrar taula arp en Windows
image::images/arpa.png[1000,1000]

.Problemes amb el protocol ARP
image::images/arp8.png[]

===== Funcionament dels Commutadors

* Els commutadors de capa 2 mantenen una *taula d'adreces MAC* que relaciona adreces MAC amb números de port.
* Segueixen el següent algoritme per reenviar les trames:
** Quan el switch rep una trama, compara la *MAC origen* amb la seva taula d'adreces MAC. Si l'*origen* és desconegut, el switch l'afegeix a la taula d'adreces MAC junt amb el número del port pel què s'ha rebut la trama.
** Seguidament el switch compara l'adreça MAC de *destí* amb les entrades de la taula. 
*** Si aquesta hi apareix, el commutador reenvia la trama especificament al port indicat pel registre de la taula.
*** En cas contrari, el commutador no sap a quin port ha d'enviar la trama i per tant procedeix a reenviar-la per *tots els ports* del switch excepte pel port per on ha entrat la trama originalment.

[IMPORTANT]
==== 
Cal notar que el switch no aprèn l'adreça de destí fins que no rep una trama d'aquell dispositiu.
====

==== Problema: Detecció i correcció d’errors

[IMPORTANT]
====
Els medis físics són propensos a errors degut a diferents tipus de pertorbacions existents, (atenuació, distorsió , soroll, etc...)

Les tècniques de detecció d'errors són necessàries per determinar si un bloc de dades transmès a través d'un medi físic ha patit algun error durant la transmissió.
====

Les estratègies per la gestió dels errors de transmissió es basen en tres idees:

* No tenir en compte els errors produïts.
* La detecció d'errors,
** Transmetre solament suficient informació per a detectar un error i tornar a demanar la informació en aquest cas.
* La correcció d'errors,
** Transmetre informació redundant que permet deduir què és el que es vol transmetre en cas d’errors en la transmissió.

===== Tècniques de detecció d'errors

Aquestes tècniques estan enfocades a la detecció d'errors de transmissió de les dades de les trames.

Un cop detectats aquets errors, en funció del protocol, es decidirà com procedir, intentar arreglar-los, ignorar-los o notificar-los.

===== Tècnica : Bits de paritat

És una de les tècniques de detecció d'errors més simple, consisteix en afegir bits de control als bits de dades amb l'objectiu de detectar els errors de transmissió.

L'emisor::
. agafa el bloc de bits que vol enviar i calcula els *bits de paritat*.
. Afegeix els bits de paritat al bloc.
. Munta la trama i l'envia al receptor.
El receptor::
. Rep la trama i pren el bloc de dades enviat per l'emissor.
. Separa els *bits de paritat* de la resta del bloc.
. Calcula els bits de paritat del bloc de dades.
. *Compara* els bits de paritat rebuts amb el bits de paritat calculats,
** Si són *iguals* considera que el bloc s'ha enviat correctament *sense errors de transmissió*.
** Si són *diferents* considera que el bloc s'ha enviat *amb errors de transmissió*.

====== Exemple - Càlcul de la lletra del DNI

La lletra del DNI s'obté a partir d'un càlcul sobre el número de DNI. S'utilitza per comprovar que no hi ha hagut errors a l'hora d'escriure un DNI.

Per una banda és dona el DNI i la lletra, per l'altra es calcula la lletra manualment.

Finalment es comparen la lletra donada i la calculada, si son iguals es considera que el DNI no té errors en cas contrari es rebutja el número de DNI.

*És molt semblant a com funcionen els bits de paritat!!*

Càlcul de la lletra del DNI:

* Considerem que el nostre alfabet té 23 lletres diferents: (Traiem la I, O, Ñ, U)
* ABCDE FGHJK LMNPQ RSTVW XYZ
* Assignem un numero de 0 a 22 a cada lletra:
* TRWAG MYFPD XBNJZ SQVHL CKE
* Dividim el número del DNI entre 23, ens donarà un resta entre 0 i 22, prenem la lletra corresponent.

===== Tècnica : Paritat Simple

[IMPORTANT]
====
La *paritat simple* d'un bloc de bits consisteix en calcular *un únic bit* a partir del bloc tenint en compte la quantitat d'*1* que té aquest bloc..

S'utilitza quan la probabilitat d'error és petita per a detectar errors de *bits individuals* en petits blocs de dades.
====
 
La paritat pot ser *senar* o *parell*:

Bit de paritat parell::
* Val 0 si el nombre d'uns que conté el bloc és parell.
* Val 1 si el nombre d'uns que conté el bloc és senar.
Bit de paritat senar::
* Val 1 si el nombre d'uns que conté el bloc és parell.
* Val 0 si el nombre d'uns que conté el bloc és senar.


Exemples:

----
Bloc: 0000000  -> 0(parell), 1(senar)
Bloc: 1011010  -> 0(parell), 1(senar)
Bloc: 0110100  -> 1(parell), 0(senar)
Bloc: 1100111  -> 1(parell), 0(senar)
----

[NOTE]
====
Aquesta tècnica s’utilitza en la comunicació a través de busos SCSI, PCI i sèrie.
====

===== Tècnica : Paritat Vertical

[IMPORTANT]
====
Aquesta tècnica s’utilitza per evitar *ràfegues d’error* a la transmissió de grans blocs.
====

. Es disposa el bloc de bits del que es vol calcular la *paritat vertical* en una taula formant *files* i *columnes*.
. Es calculen les *paritats simples* de cada fila i de cada columna.
. Es calcula la *paritat simple* de les paritats obtingudes en el cas anterior.
. El conjunt ordenat segons indiqui el protocol forma la *paritat vertical* del bloc de dades.
. La *paritat vertical* pot corregir un bit erroni per fila i per columna.

Exemple:

----
Bloc de dades:   01100011 00101011 01001110 11011011 10000001 10000000 11010011 
Càlcul dels bits de paritat vertical senar:

  01100011 1
  00101011 1
  01001110 1
  11011011 1
  10000001 1
  10000000 0
  11010011 0
  
  11110000 0
  
Bits de paritat: 11110000 1111100 0
----

===== Tècnica : Còdis CRC

Un dels codis de detecció d’errors més utilitzats actualment són els codis CRC (Cyclic Redudancy Check) basats en divisions de polinomis.

A la pràctica s’utilitza la *divisió polinomial* per afegir un conjunt de bits de paritat que controlen errors de major multiplicitat.

===== Tècniques de correcció d’errors

Les tècniques basades en la correcció d'errors intenten proporcionar prou informació per reparar els errors de transmissió automàticament en cas de produir-se. 

===== Tècnica : Repetició

Una de les tècniques anomenada *de repetició* consisteix en repetir els bits a enviar varies vegades.

Exemple:

* Codi de repetició n=3
* Consisteix en enviar cada bit 3 vegades.
* Per enviar un 1 s’envia 111 i per enviar un 0 s’envia 000.
* El receptor interpreta un 0 o un 1 en funció del dígit que aparegui més vegades.

----
Es vol enviar: 11001
S'envia: 111111000000111
----

===== Tècniques de control d’errors

Les tècniques de control d'errors pretenen notificar a l'emissor que el receptor ha rebut un conjunt de dades amb errors.

Per exemple:

* Proporcionar retroalimentació a l’emissor sobre el que està passant a l’altra banda de la línia.
* En general el receptor envia trames especials de control indicant si la trama ha arribat correctament.
* Què passa si la trama de confirmació no es rep?
* Cal posar temporitzadors als emissors, a la capa d’enllaç,  per evitar que estiguin esperant indefinidament una trama de recepció que mai arribarà.
* Quan el temporitzador caduca es torna a enviar la trama amb el risc que arribin trames repetides.

==== Problema: Accés al medi compartit

La *subcapa MAC* s’encarrega de determinar qui utilitza el canal quan hi ha competència. (pe. en xarxes de Broadcast)

Per assignar una sol canal de broadcast existeixen dos esquemes:

Esquema estàtic::
L’assignació de permisos per utilitzar el canal està prèviament determinat.
Esquema dinàmic::
L’assignació de permisos per utilitzar el canal s’estableix segons les necessitats de cada moment.

===== Esquema estàtic

Les dues estratègies principals són:

* S’utilitza alguna tècnica de *multiplexació per divisió de freqüència* per dividir l’ample de banda en N canals.
* S’utilitza alguna tècnica de *multiplexació per temps* dividir l’ample de banda en N canals.

Els inconvenients d'aquestes tècniques són:

* Si menys de N usuaris volen usar el canal, es perd ample de banda.
* Si més de N usuaris volen usar el canal, es nega servei a alguns, tot i que hi ha usuaris que no usen els seus amples de banda.
* Perquè el tràfic en sistemes computacions ocorre en ràfegues, molts dels subcanals estaran desocupats per molt del temps.

===== Esquema dinàmic

Atès que cap dels mètodes tradicionals de repartiment estàtic de canal funciona massa bé amb tràfic en ràfegues se solen aplicar mètodes dinàmics pel repartiment del canal.

Les estratègies principals són:

Reserva::
El temps es divideix en ranures, una estació que desitgi transmetre ha de reservar un nombre d'elles .
Round robin::
A cada estació seguint un torn se li dóna l'oportunitat de transmetre.
Amb contesa (lluita)::
No es controla de qui és el torn de transmetre, totes les estacions lluiten per un temps.
Lliures de col·lisió::
Que resolen la contenció pel canal sense que hagi col·lisions, ni tan sols durant el període de contenció; entre ells estan: mapa de bits i “conteig” descendent binari.

[NOTE]
====
Pel tràfic a ràfegues s'adapten millor els mecanismes de round robin i contesa. Entre aquests últims i en condicions de càrrega baixa són preferibles els mètodes de contenció, mentre que si la càrrega és alta són més eficaços els protocols lliures de col·lisions.
====
//// 

////
===  Subcapa MAC – Media Access Control

*   La majoria de protocols es basen en els següents supòsits:
*   *   Model d’estació
            *   El model consisteix en N estacions independents que poden generar trames per a la transmissió. Suposem que un cop s’ha generat la trama l’estació es bloqueja i no envia res més fins que la trama no s’ha transmès amb èxit.
    *   Suposem un únic canal
            *   Hi ha un sol canal disponible per a totes les comunicacions. Totes les estacions poden transmetre o rebre informació pel canal.

===  Subcapa MAC – Media Access Control

*   La majoria de protocols es basen en els següents supòsits: (cont)
*   *   Sobre les col·lisions
            *   Si dues trames es transmeten de forma simultània es superposen i el senyal resultant s’altera. Aquest fet s’anomena col·lisió. Totes les estacions poden detectar col·lisions. Una trama en col·lisió s’ha de tornar a enviar. Suposem que no hi ha més errors a part de les col·lisions.
    *   Temps
            *   Continu - No hi ha un rellotge mestre que divideixi el temps en intervals.
                *   Fragmentat

*   Detecció de portadores
*   *   Amb detecció de portadora
            *   Les estacions poden saber si el canal està en us abans d’intentar utilitzar-lo.
    *   Sense detecció de portadora
            *   Les estacions no poden detectar el canal fins que no han emés una trama. Només en aquest cas poden determinar si la transmissió ha tingut èxit.

===  Subcapa MAC - Protocols

*   Protocol bàsic de mapa de bits
  *   És un  protocol lliure de col·lisions.
        *   Suposem que hi ha N estacions, cadascuna amb una adreça única de 0 a N - 1 incorporada en maquinari.
        *   Cada període de contenció consisteix en exactament N ranures.
        *   Si l'estació 0 té una trama per enviar, transmet un bit durant la ranura 0. No està permès a cap altra estació transmetre durant aquest interval.
        *   Sense importar el que faci l'estació 0, l'estació 1 té l'oportunitat de transmetre un 1 durant la ranura 1, però només si té en cua una trama. En general l'estació j pot anunciar que té una trama per enviar introduint un bit 1 en la ranura j.

===  Subcapa MAC - Protocols

*   Protocol bàsic de mapa de bits (cont)
   *
    *   Una vegada que han passat les N ranures, cada estació té coneixement complet de les estacions que volen transmetre. En aquest punt, les estacions comencen a transmetre en ordre numèric.

        *   Atès que tots estan d'acord en qui continua, mai haurà col·lisions. Una vegada que l'última estació hagi transmès la seva trama, esdeveniment que poden detectar fàcilment totes les estacions, comença altre període de contenció de N bits.

        *   Si una estació queda té informació per transmetre just després que ha passat la seva ranura de bit, ha tingut mala sort i haurà de romandre callada fins que cada estació hagi tingut la seva oportunitat i el mapa de bits hagi començat de nou.

===  Subcapa MAC - Protocols

*   Protocols amb utilització d’un testimoni (token)
   *   És una particularització de reserva de canal consistent en un missatge especial que es va passant d'estació a estació.
        *   Cada estació pot transmetre el seu missatge solament quan està en possessió del testimoni, cosa que ocorre quan rep el que es denomina el testimoni lliure.
        *   Els testimonis poden ser de diversos tipus, depenent de la xarxa local.
        *   Un testimoni lliure pot ser un missatge d'alguns caràcters que s'intercanvien les estacions. Quan una estació que vol transmetre rep el testimoni lliure genera un testimoni ocupat. En aquest s'inclouen les adreces de l'estació origen i destí i precedeix a les dades.
        *   Per a evitar situacions de bloqueig a causa del tipus d'operació del pas de testimoni, les xarxes que utilitzen aquest mètode d'accés solen tenir una estació que fa de controlador de situacions d'error.

===  Subcapa MAC - Protocols
*   Protocols amb utilització d’un testimoni (token)
*   Existeixen 2 modalitats d'utilització de testimoni:

    *   Pas de testimoni
            *   Aquest sistema evita que tractin de comunicar-se més d'un node simultàniament passant un paquet especial, testimoni, a través de la xarxa, sent aquest testimoni el qual dóna el permís de transmetre.
                *   Quan un equip vol comunicar-se ha d'agafar prèviament el testimoni, i quan acaba allibera el testimoni de manera que es pot establir una nova comunicació.
      *   Captura de testimoni

            *   Aquest sistema, utilitzat en les xarxes FDDI, és semblant al mètode anterior amb l'excepció que per a transmetre no cal esperar que quedi lliure el testimoni, simplement s’afegeix la informació al testimoni que circula.

*
////

////
==== Família de protocols ALOHA

[IMPORTANT]
====
*ALOHAnet* va ser la primera xarxa de dades sense fils operativa.
====

* Principis de la dècada de 1970, la universitat de Hawaii està interessada en connectar diferents usuaris d’illes remotes a la computadora principal de Honolulu, el sistema telefònic per la zona era pràcticament inexistent i passar cables sota l’Oceà Pacífic no era viable.
* L'objectiu era utilitzar equipament estàndard de baix cost de comunicació per radio.
* La solució varen ser les ones de radio d’ona curta. Cada terminal estava equipada amb una petita radio de dues freqüències: un canal ascendent a la computadora central i un canal descendent des de la computadora central.
* La importància d'ALOHAnet és deguda a la utilització d'un *canal compartit* per a les transmissions dels clients.
* El de protocol de xarxa es va dissenyar específicament pel tràfic interactiu. En un sistema convencional s'hauria implementat alguna tècnica de multiplexació per temps o per freqüència, no obstant aquests sistemes malgasten una gran quantitat d'ample de banda per comunicacions a "ràfegues" com pot ser el tràfic interactiu.

ifdef::slides[<<<]
ifdef::slides[=== Protocol ALOHA pur (1971) ]
ifndef::slides[==== Protocol ALOHA pur (1971)]

* Els usuaris transmeten pel canal ascendent quan tenen alguna cosa per enviar.
* Esperen la confirmació de recepció pel canal descendent.
* Si la confirmació no arriba entenen que hi ha hagut una col·lisió i reenvien la trama més tard.
*  El temps que tarden en reenviar la trama ha de ser aleatori per què si no hi tornaria ha haver una col·lisió.
* La eficiència del protocol ALOHA pur és molt baixa.
* El rendiment de la xarxa és d'un 18,4%

ifdef::slides[<<<]
ifdef::slides[=== Protocol ALOHA fragmentat (1972)]
ifndef::slides[==== Protocol ALOHA fragmentat (1972)]

* Es divideix el temps en intervals discrets equivalents al temps que una estació triga a emetre una trama i a rebre la confirmació de recepció.
* La estació central emet un senyal al principi de cada interval de temps.
* Les estacions només poden emetre al principi de cadascun d’aquests intervals.
* Les col·lisions es redueixen respecte la versió anterior.
* El rendiment de la xarxa és d'un 36,8%
////

////
==== Familia de protocols CSMA (Carrier Sense Multiple Access)

CSMA és un protocol de capa 2 que s'utilitza en topologies amb mitja de transmissió compartit, com per exemple Ethernet.

El nom del protocol resumeix les característiques generals de la família de protocols CSMA:

Carrier Sense::
Significa que els dispositius connectats al cable de xarxa poden "escoltar" el canal abans de transmetre.
Multiple Access::
Significa que es pot connectar al segment de xarxa més d'un dispositiu.
El protocols CSMA és va desenvolupar per resoldre el problema principal d'ALOHA, *minimitzar les probabilitats de col·lisió* 

===== Protocol CSMA persistent (1975)

* Quan una estació té dades per enviar primer escolta el canal per veure si una altra està transmetent en aquell moment.
* Si el canal està ocupat, *espera* fins que el canal s'allibera.
* Quan l'estació detecta que el canal està buit envia immediatament la trama.
* Si hi ha una col·lisió, l’estació espera un temps aleatori i torna a començar.
* Inconvenient principal d'aquesta versió:
El retard de propagació::
Hi ha una petita possibilitat que, just desprès que una estació comença a emetre, una altra estació està llesta per enviar i detectar el canal. Si el senyal de la primera estació no ha arribat a la segona, aquesta detectarà el canal inactiu i començarà a enviar produint una col·lisió.

===== Protocol CSMA no persistent

* Quan una estació té una trama per enviar primer escolta el canal per veure si una altra està transmetent en aquell moment.
* Si el canal està ocupat, *espera un temps aleatori* i torna a començar.
* Si no, es posa ha enviar immediatament.
* Si hi ha una col·lisió, l’estació espera un temps aleatori i torna a començar
* Avantatge d'aquesta versió:
** Redueix les possibilitats de col·lisió perquè les estacions esperen un temps aleatori abans de transmetre. És més difícil que dues estacions que tenen dades per enviar parlin a la vegada.
* Inconvenient d'aquesta versió:
** Redueix la eficiència de la xarxa perquè el canal pot quedar desocupat havent-hi estacions que tenen dades per enviar.

===== Protocol CSMA amb detecció de col·lisions CSMA/CD

* Quan una estació té una trama per enviar primer escolta el canal per veure si una altra està transmetent en aquell moment.
* Si el canal està ocupat, espera un temps aleatori i torna a començar.
* Si no, comença a enviar la trama pendent *escoltant el canal* i mirant si el que està enviant és el mateix que el que està escoltant, si no és així interromp immediatament la comunicació, espera un temps aleatori  i torna a començar el procés. (si hi torna a haver una col·lisió el temps d’espera aleatori es duplica, i així successivament, fins a un moment en que l'emissor decideix avortar l'enviament i descartar la trama)

[IMPORTANT]
====
Aquesta versió del protocol és important per què és la utilitzada per les xarxa *Ethernet*
====

ifdef::slides[<<<]
ifdef::slides[=== Protocol CSMA amb detecció de col·lisions CSMA/CD]

[NOTE]
====
En el cas CSMA/CD la codificació del senyal es complicada, ja que si un 0 és un senyal de 0 volts, com puc detectar una col·lisió de dos 0?
====

[NOTE]
====
En el protocol CSMA/CD els períodes de contesa són un problema important quan:

* Els cables són llargs, ja que el retard de propagació és major, i les trames són més curtes (la sobrecàrrega dels períodes de contesa juga un paper més important.
* Precisament aquest és el cas en les xarxes de fibra òptica.
* Hi ha protocols que permeten eliminar completament la possibilitat de xocs en el període de contesa. (pe. protocol bàsic de mapa de bits)
==== 

==== Mitjans sense fils

[WARNING]
====
Utilitzar CSMA/CD en xarxes sense fils *no és una bona tàctica* ja que serveix per detectar col·lisions prop de l’emissor i en el cas sense fils interessa detectar-les prop del receptor.
====
    
En la comunicació sense fils es donen tres problemes:

. La majoria de ràdios són semidúplex i per tant no es pot emetre i rebre a l’hora.
Problema del node ocult::
L’estació creu que el mitjà està lliure quan en realitat  està utilitzat per un altre node que l’estació no detecta per estar fora d’abast. +

.Problema del node ocult
image::images/nodeocult.png[1000,1000]

Problema del node exposat::
L’estació pensa que el mitjà està ocupat quan en realitat l’està ocupant un altre node que no interferiria la transmissió perquè utilitza un receptor diferent. +

.Problema del node exposat
image::images/nodeexposat.png[1000,1000]

==== Protocol MACA – multiple access with collition avoidance

Protocol MACA (1990) és un dels primers protocols dissenyats per LANs sense fils.

* Suposem que una estació A envia una trama a una estació B.
* A envia una trama *RTS* a B (Request to send – petició d’enviament) molt curta (30 bytes) indicant la longitud de la trama de dades que seguirà.
* Totes les estacions que “sentin” la trama RTS (excepte B) hauran d'estar en silenci fins que acabi la transmissió.
* B respon a A amb una trama *CTS* (Clear to send – permís per enviar) amb la longitud de les dades a enviar.
* Totes les estacions que “sentin” la trama CTS (excepte A) hauran d'estar en silenci fins que acabi la transmissió. I així s'evita el problema del node ocult.
* A rep CTS i comença a transmetre.
* Les estacions que reben la trama RTS i la trama CTS estaran en silenci fins que han calculat que la trama ha estat enviada i rebuda.

Una versió millorada d’aquest protocol, anomenada *CSMA/CA* (*Carrier sense multiple access with collision avoidance*)  és la que utilitzen les xarxes IEEE 802.11 (Wifi)
////

=== Ethernet

Bob Metcalfe basant-se en el treball de Norman Abranson, l’inventor d’ALOHANET, va dissenyar un nou model de xarxa que anomenà *Ethernet*.

En aquest cas el medi de transmissió no era el buit, sinó un cable coaxial gruixut (anomenat 10Base5) de mes de 2,5 km de llarg amb repetidors cada 500m.

El sistema podia contenir fins a 256 màquines acoblades al cable, funcionava amb un protocol *CSMA amb detecció de col·lisions* i codificava les dades amb la *codificació  Manchester* que elimina la necessitat de sincronització.


Ethernet no és un protocol, és un conjunt d’estàndards i admet diferents medis, amples de banda i algunes variacions de les capes 1 i 2. *No obstant el format de trama i l’adreçament és el mateix en totes les varietats*.

[IMPORTANT]
====
*Per tant sempre que ens mantinguem en xarxes Ethernet el format de la trama no canvia.*
====

====  Format d’una trama Ethernet

.format de la trama Ethernet
image::images/tramaethernet.png[]

Preàmbul (56 bits)::
Seqüència de 1 i 0 per a la sincronització.
* 10101010 10101010 10101010 10101010 10101010 10101010 10101010
* Els sniffers no mostren el camp perquè aquests bits són eliminats per l’adaptador de xarxa al passar la trama al host.
SFD (Start-of-frame-delimiter) (8 bits)::
Indicador d’inici de trama.
* 10101011
* Els sniffers no mostren el camp perquè aquests bits són eliminats per l’adaptador de xarxa al passar la trama al host.
Adreça MAC origen (48 bits)::
Adreça física origen d ela trama
Adreça MAC destí (48 bits)::
Adreça física destí de la trama
Ethertype/longitud (16 bits)::
El significat d’aquest camp depèn de la versió de l’estàndard Ethernet que es prengui.
* IEEE 802.3 (més modern): Indica la longitud de la trama en bytes.
* El camp pren valors entre 64 i 1522 (en decimal). Això permet a la capa d’enllaç saber quina d eles dues versions de l’estàndard s’està utilitzant.
Ethernet II::
Indica el protocol que està encapsulat en el camp de dades.
* El camp pren valors a partir de 1536 (0x0600).
* Exemples:
** 0x0800 -> Ipv4 
** 0x0806 -> ARP
** 0x809B -> Appletalk
** 0x8137 -> Novell IPX
** 0x86DD -> Ipv6
CRC (32 bits)::
CRC del contingut de la trama per permetre la detecció d'errors de transmissió
Interframe gap::
Quan s’envia una trama Ethernet cal enviar 12 octets a zero abans de transmetre la següent.      