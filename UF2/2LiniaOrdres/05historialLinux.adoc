== Comandes Linux - Treballar amb l'historial

=== Repetir la última comanda

Per repetir la última comanda a bash, teclejar *!!*

[NOTE]
====
En anglès *!* s'anomena *bang*.
====

[source,bash]
----
josep@odin:~$ echo patata > file.txt
josep@odin:~$ !!
echo patata > file.txt
----

=== Repetir altres comandes

Per repetir la última comanda entrada que comenci per un o més caràcters *!_caracters_*

[source,bash]
----
josep@odin:~$ touch file
josep@odin:~$ cat file
josep@odin:~$ !to
touch file
----

=== ((history))

Permet veure les comandes entrades anteriorment.

*history n* mostra les n últimes comandes.

[source,bash]
----
josep@odin:~$ history 10
  373  help | head -15
  374  echo patata > file.txt
  375  touch file
  376  cat file
  377  |to
  378  touch file
  379  history
  380  ping
  381  history 
  382  history 10
----

=== ((!n))

Escriure *!* seguit d'un número repeteix la comanda emmagatzemada a l'historial amb aquest número.

[source,bash]
----
josep@odin:~$ !378
touch file
----

=== Ctrl-r

[CTRL]+r seguit d'alguns caràcters, busca l'última comanda que comenci per aquests caràcters a l'historial.

[source,bash]
----
(reverse-i-search)`apt': sudo apt install whois
----

=== (($HISTSIZE))

La variable *$HISTSIZE* determina el nombre de comandes que es poden emmagatzemar a l'historial, *durant una sessió bash*.

La majoria de distribucions tenen aquest paràmetre a 500 - 1000 per defecte.

[source,bash]
----
josep@odin:~$ echo $HISTSIZE
1000
----

[source,bash]
----
josep@odin:~$ HISTSIZE=0
josep@odin:~$ echo $HISTSIZE
0
josep@odin:~$ history
----

=== (($HISTFILE))

La variable *$HISTFILE* apunta al fitxer on s'emmagatzema l'historial. Per defecte *~/.bash_history*.

[source,bash]
----
josep@odin:~$ echo $HISTFILE
/home/josep/.bash_history
----

[WARNING]
====
L'historial de la sessió s'emmagatzema en aquest fitxer en el moment de tancar la sessió.

Tancar un terminal de gnome amb el ratolí o llançar la comanda *reboot* NO guardarà l'historial al fitxer.
====

=== (($HISTFILESIZE))

El nombre de comandes que es poden emmagatzemar a l'historial es pot establir modificant la variable *$HISTFILESIZE*, *es manté l'historial entre sessions bash*.

[source,bash]
----
josep@odin:~$ echo $HISTFILESIZE 
2000
----

=== Evitar emmagatzemar una comanda a l'historial

Començar una comanda amb un *espai* evita que aquesta quedi guardada a l'historial.

=== Expressions regulars

((TODO))

////
És possible utilitzar expressions regulars al utilitzar *!*

[source,bash]
----
usuari@Azathoth:~/test$ cat file1
usuari@Azathoth:~/test$ !c:s/1/2 # Busca un 1 i hi posa un 2
cat file2
hello
usuari@Azathoth:~/test$
----
////

=== Historial a la shell Korn

((TODO))

////
L'historial de la shell de Korn és similar a la de Bash però les comandes tenen petites variacions.

El paràmetre de history s'interpreta diferent.

Repetir una línia de l'historial utilitza la lletra *r* enlloc de *!*.

[source,bash]
----
$ history 17
17 clear
18 echo hoi
19 history 12
20 echo world
21 history 17
----

[source,bash]
----
$ r e
echo world
world
$ cd /etc
$ r
cd /etc
$
----
////
